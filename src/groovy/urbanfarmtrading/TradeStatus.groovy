package urbanfarmtrading

public enum TradeStatus {

    WAITING_AGREEMENT(10, 'tradeStatus.code.WAITING_AGREEMENT'),
    WAITING_SENDINGS(20, 'tradeStatus.code.WAITING_SENDINGS'),
    WAITING_ARRIVALS(30, 'tradeStatus.code.WAITING_ARRIVALS'),
    WAITING_REVIEWS(40, 'tradeStatus.code.WAITING_REVIEWS'),
    SUCCESS(50, 'tradeStatus.code.SUCCESS'),
    FAILURE(60, 'tradeStatus.code.FAILURE')

    Long id
    String code

    TradeStatus(Long id, String code) {
        this.id = id
        this.code = code
    }

    String toString() {
        code
    }
}