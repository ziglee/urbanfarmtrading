package urbanfarmtrading

public enum FoodType {

    FLOWER(10, 'food.type.FLOWER'),
    FRUIT(20, 'food.type.FRUIT'),
    FUNGUS(30, 'food.type.FUNGUS'),
    HERB(40, 'food.type.HERB'),
    LEGUME(50, 'food.type.LEGUME'),
    NUT(60, 'food.type.NUT'),
    SEED(70, 'food.type.SEED'),
    VEGETABLE(80, 'food.type.VEGETABLE'),
    ANIMAL(90, 'food.type.ANIMAL')

    Long id
    String code

    FoodType(Long id, String code) {
        this.id = id
        this.code = code
    }

    String toString() {
        code
    }
}