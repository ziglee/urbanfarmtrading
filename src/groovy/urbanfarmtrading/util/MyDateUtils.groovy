package urbanfarmtrading.util

class MyDateUtils {

    def static DATE_FORMAT = 'dd/MM/yyyy'
    def static DATE_HOUR_FORMAT = 'dd/MM/yyyy HH:mm'

    public static Date[] daterange(String daterange) {
        def start
        def end
        if (daterange) {
            String[] split = daterange.split(' - ')
            try {
                start = Date.parse('dd/MM/yyyy', split[0])
                if (split.length > 1)
                    end = Date.parse('dd/MM/yyyy', split[1]) + 1
                else
                    end = start + 1
            } catch (java.text.ParseException pe) {}
        }
        return [start, end]
    }

    public static Date date(String date) {
        Date.parse(DATE_FORMAT, date)
    }

    public static Date dateHour(String dateHour) {
        Date.parse(DATE_HOUR_FORMAT, dateHour)
    }
}
