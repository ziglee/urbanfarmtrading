package urbanfarmtrading.util

class MyNumberParseUtils {

    public static Double asDouble(String str) {
        if (str) {
            str = str.replace('.', '')
            str = str.replace(',', '.')
            try {
                return Double.parseDouble(str)
            } catch (NumberFormatException nfe) {}
        }
        return null
    }
}
