package urbanfarmtrading.util

class RandomPasswordGenerator {

    private static final char[] alfaLower = 'a'..'z'
    private static final char[] alfaUpper = 'A'..'Z'
    private static final char[] numeric = '0'..'9'

    public static String generate() {
        StringBuilder password = new StringBuilder()
        def random = new Random()
        (0..5).each {
            def nextChar
            def nextInt = random.nextInt(3)
            if (nextInt == 0)
                nextChar = alfaLower[random.nextInt(alfaLower.length)]
            else if (nextInt == 1)
                nextChar = alfaUpper[random.nextInt(alfaUpper.length)]
            else
                nextChar = numeric[random.nextInt(numeric.length)]
            password.append(nextChar)
        }
        return password.toString()
    }
}
