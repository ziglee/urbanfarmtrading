A social network targeted at Urban Homesteads who wants to trade their surplus production with other Urban Homesteads.
* * *
### Keywords
* Urban farms
* Edible landscaping
* Self-sufficient living
* Community food-sourcing
* Food preservation
* Resource reduction