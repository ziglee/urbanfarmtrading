databaseChangeLog = {

    changeSet(author: "cassio (generated)", id: "1387139031650-1") {
        createTable(tableName: "admin_user") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "admin_userPK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "login", type: "varchar(255)") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "varchar(255)") {
                constraints(nullable: "false")
            }

            column(name: "password", type: "varchar(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-2") {
        createTable(tableName: "food") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "foodPK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "varchar(150)") {
                constraints(nullable: "false")
            }

            column(name: "photo_filename", type: "varchar(255)")

            column(name: "type", type: "bigint") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-3") {
        createTable(tableName: "food_demand") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "food_demandPK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "food_id", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "user_profile_id", type: "bigint") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-4") {
        createTable(tableName: "food_offer") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "food_offerPK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "available_until", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "comment", type: "varchar(400)")

            column(name: "food_id", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "kilograms", type: "float") {
                constraints(nullable: "false")
            }

            column(name: "user_profile_id", type: "bigint") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-5") {
        createTable(tableName: "food_portion_trade") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "food_portion_PK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "food_id", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "from_engaging", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "kilograms", type: "float") {
                constraints(nullable: "false")
            }

            column(name: "trade_id", type: "bigint") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-6") {
        createTable(tableName: "trade") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "tradePK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "engaged_agreed", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "engaged_positive_review", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "engaged_received", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "engaged_reviewed", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "engaged_sent", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "engaged_user_id", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "engaging_agreed", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "engaging_positive_review", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "engaging_received", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "engaging_reviewed", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "engaging_sent", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "engaging_user_id", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "status", type: "bigint") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-7") {
        createTable(tableName: "user_profile") {
            column(autoIncrement: "true", name: "id", type: "bigint") {
                constraints(nullable: "false", primaryKey: "true", primaryKeyName: "user_profilePK")
            }

            column(name: "version", type: "bigint") {
                constraints(nullable: "false")
            }

            column(name: "activation_code", type: "varchar(255)")

            column(name: "active", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "address", type: "varchar(500)") {
                constraints(nullable: "false")
            }

            column(name: "change_password_code", type: "varchar(255)")

            column(name: "email", type: "varchar(250)") {
                constraints(nullable: "false")
            }

            column(name: "email_confirmed", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "has_offers", type: "bit") {
                constraints(nullable: "false")
            }

            column(name: "latitude", type: "double precision")

            column(name: "longitude", type: "double precision")

            column(name: "name", type: "varchar(250)") {
                constraints(nullable: "false")
            }

            column(name: "password", type: "varchar(255)") {
                constraints(nullable: "false")
            }

            column(name: "photo_filename", type: "varchar(255)")

            column(name: "rating", type: "float") {
                constraints(nullable: "false")
            }

            column(name: "rating_count", type: "bigint") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-16") {
        createIndex(indexName: "login_uniq_1387139031527", tableName: "admin_user", unique: "true") {
            column(name: "login")
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-17") {
        createIndex(indexName: "name_uniq_1387139031538", tableName: "food", unique: "true") {
            column(name: "name")
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-18") {
        createIndex(indexName: "FK6E6FD54C6387C5CE", tableName: "food_demand") {
            column(name: "user_profile_id")
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-19") {
        createIndex(indexName: "FK6E6FD54CFB52814F", tableName: "food_demand") {
            column(name: "food_id")
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-20") {
        createIndex(indexName: "FKFBE94B3B6387C5CE", tableName: "food_offer") {
            column(name: "user_profile_id")
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-21") {
        createIndex(indexName: "FKFBE94B3BFB52814F", tableName: "food_offer") {
            column(name: "food_id")
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-22") {
        createIndex(indexName: "FKF1D810EBA4E5FD85", tableName: "food_portion_trade") {
            column(name: "trade_id")
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-23") {
        createIndex(indexName: "FKF1D810EBFB52814F", tableName: "food_portion_trade") {
            column(name: "food_id")
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-24") {
        createIndex(indexName: "FK697F1646F14505C", tableName: "trade") {
            column(name: "engaged_user_id")
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-25") {
        createIndex(indexName: "FK697F1647D8A7817", tableName: "trade") {
            column(name: "engaging_user_id")
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-26") {
        createIndex(indexName: "email_uniq_1387139031562", tableName: "user_profile", unique: "true") {
            column(name: "email")
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-27") {
        createIndex(indexName: "name_uniq_1387139031564", tableName: "user_profile", unique: "true") {
            column(name: "name")
        }
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-8") {
        addForeignKeyConstraint(baseColumnNames: "food_id", baseTableName: "food_demand", constraintName: "FK6E6FD54CFB52814F", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "food", referencesUniqueColumn: "false")
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-9") {
        addForeignKeyConstraint(baseColumnNames: "user_profile_id", baseTableName: "food_demand", constraintName: "FK6E6FD54C6387C5CE", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user_profile", referencesUniqueColumn: "false")
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-10") {
        addForeignKeyConstraint(baseColumnNames: "food_id", baseTableName: "food_offer", constraintName: "FKFBE94B3BFB52814F", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "food", referencesUniqueColumn: "false")
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-11") {
        addForeignKeyConstraint(baseColumnNames: "user_profile_id", baseTableName: "food_offer", constraintName: "FKFBE94B3B6387C5CE", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user_profile", referencesUniqueColumn: "false")
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-12") {
        addForeignKeyConstraint(baseColumnNames: "food_id", baseTableName: "food_portion_trade", constraintName: "FKF1D810EBFB52814F", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "food", referencesUniqueColumn: "false")
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-13") {
        addForeignKeyConstraint(baseColumnNames: "trade_id", baseTableName: "food_portion_trade", constraintName: "FKF1D810EBA4E5FD85", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "trade", referencesUniqueColumn: "false")
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-14") {
        addForeignKeyConstraint(baseColumnNames: "engaged_user_id", baseTableName: "trade", constraintName: "FK697F1646F14505C", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user_profile", referencesUniqueColumn: "false")
    }

    changeSet(author: "cassio (generated)", id: "1387139031650-15") {
        addForeignKeyConstraint(baseColumnNames: "engaging_user_id", baseTableName: "trade", constraintName: "FK697F1647D8A7817", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user_profile", referencesUniqueColumn: "false")
    }
}
