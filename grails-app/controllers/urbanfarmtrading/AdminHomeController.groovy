package urbanfarmtrading

import grails.transaction.Transactional

@Transactional(readOnly = true)
class AdminHomeController {

    static allowedMethods = [authenticate: "POST"]

    def index() {
        if (session.adminUser) {
            redirect(controller: 'adminDashboard')
            return
        }
        redirect(action: "login", params: params)
    }

    def login() {}

    def logout() {
        session.invalidate()
        redirect(uri: '/')
    }

    def authenticate() {
        def adminUser = AdminUser.findByLogin(params.login)
        if (adminUser) {
            if (params.password.encodeAsSHA256().equals(adminUser.password)) {
                session.adminUser = [id: adminUser.id, name: adminUser.name, login: adminUser.login]
                redirect(controller: 'adminDashboard', action: 'index')
                return
            }
        }
        flash.message = "Ops... ${params.login} por favor tente novamente."
        redirect(action: "login")
    }
}
