package urbanfarmtrading

import grails.converters.JSON
import grails.transaction.Transactional

import java.text.NumberFormat

import org.joda.time.DateTime

@Transactional(readOnly = true)
class UserProfileDashboardController {

    public static final LAT_LON_FORMAT = NumberFormat.getNumberInstance(new Locale('en'))

    FoodOfferService foodOfferService

    static {
        LAT_LON_FORMAT.groupingUsed = false
    }

    def index() {
        def userProfile = UserProfile.get(session.userProfile.id)

        def foodOffers = FoodOffer.executeQuery('select s from FoodOffer as s where s.availableUntil >= :now and s.userProfile.id = :userProfileId order by s.food.name asc', [userProfileId: userProfile.id, now: new DateTime().withTimeAtStartOfDay().toDate()])

         def foodDemands = FoodDemand.executeQuery('select s from FoodDemand as s where s.userProfile.id = :userProfileId order by s.food.name asc', [userProfileId: userProfile.id])

         def trades = Trade.executeQuery('select t from Trade as t where (t.engagedUser.id = :userProfile or t.engagingUser.id = :userProfile) and t.status <> :success and t.status <> :failure order by t.lastUpdated desc', [userProfile: session.userProfile.id, success: TradeStatus.SUCCESS, failure: TradeStatus.FAILURE])

        [userProfile: userProfile, latLongFormat: LAT_LON_FORMAT, foodOffers: foodOffers, foodDemands: foodDemands, trades: trades]
    }

    def listUserProfilesByLatLngBounds(double northEastLatitude, double northEastLongitude,
                                       double southWestLatitude, double southWestLongitude) {
        def hql = 'select s from UserProfile as s where s.id != :userProfileId and s.active = true and s.hasOffers = true '
        hql += " and s.latitude <= :northEastLatitude and s.latitude >= :southWestLatitude "
        hql += " and s.longitude <= :northEastLongitude and s.longitude >= :southWestLongitude "
        hql += " and s.latitude <> 0 and s.longitude <> 0 "
        hql += " order by s.rating desc "

        def hqlParams = [:]
        hqlParams.put 'userProfileId', session.userProfile.id
        hqlParams.put 'northEastLatitude', northEastLatitude
        hqlParams.put 'northEastLongitude', northEastLongitude
        hqlParams.put 'southWestLatitude', southWestLatitude
        hqlParams.put 'southWestLongitude', southWestLongitude

        def usersNearby = UserProfile.executeQuery(hql, hqlParams, [max: 30]).collect {
            [id: it.id, name: it.name, latitude: LAT_LON_FORMAT.format(it.latitude), longitude: LAT_LON_FORMAT.format(it.longitude), url: createLink(controller: 'userProfile', action: 'show', id: it.id)]
        }

        def result = [error: 0, payload: usersNearby]
        render result as JSON
    }

    def listUserProfilesWhoFulfillMyDemandByLatLngBounds(double northEastLatitude, double northEastLongitude,
                                                         double southWestLatitude, double southWestLongitude) {
        def userProfile = UserProfile.get(session.userProfile.id)

        def usersNearby = foodOfferService.whoFulfillDemand(userProfile, northEastLatitude, northEastLongitude,
                southWestLatitude, southWestLongitude).collect {
            [id: it.id, name: it.name, latitude: LAT_LON_FORMAT.format(it.latitude), longitude: LAT_LON_FORMAT.format(it.longitude), url: createLink(controller: 'userProfile', action: 'show', id: it.id)]
        }

        def result = [error: 0, payload: usersNearby]
        render result as JSON
    }
}
