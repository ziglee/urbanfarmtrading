package urbanfarmtrading

import grails.transaction.Transactional

@Transactional(readOnly = true)
class AdminDashboardController {

    def index() { }
}
