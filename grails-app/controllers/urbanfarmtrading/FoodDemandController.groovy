package urbanfarmtrading

import grails.transaction.Transactional
import org.springframework.dao.DataIntegrityViolationException

@Transactional(readOnly = true)
class FoodDemandController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def create() {
        [foodDemandInstance: new FoodDemand(params)]
    }

    @Transactional
    def save() {
        def foodDemandInstance = new FoodDemand(params)
        foodDemandInstance.userProfile = UserProfile.get(session.userProfile.id)

        if (!foodDemandInstance.save(flush: true)) {
            render(view: "create", model: [foodDemandInstance: foodDemandInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'foodDemand.label', default: 'FoodDemand'), foodDemandInstance.food.name])

        redirect(controller: 'userProfileDashboard', action: 'index')
    }

    def edit(Long id) {
        def foodDemandInstance = FoodDemand.get(id)
        if (!foodDemandInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'foodDemand.label', default: 'FoodDemand'), id])
            redirect(controller: 'userProfileDashboard', action: 'index')
            return
        }

        [foodDemandInstance: foodDemandInstance]
    }

    @Transactional
    def update(Long id, Long version) {
        def foodDemandInstance = FoodDemand.get(id)
        if (!foodDemandInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'foodDemand.label', default: 'FoodDemand'), id])
            redirect(controller: 'userProfileDashboard', action: 'index')
            return
        }

        if (version != null) {
            if (foodDemandInstance.version > version) {
                foodDemandInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'foodDemand.label', default: 'FoodDemand')] as Object[],
                        "Another user has updated this FoodDemand while you were editing")
                render(view: "edit", model: [foodDemandInstance: foodDemandInstance])
                return
            }
        }

        foodDemandInstance.properties = params

        if (!foodDemandInstance.save(flush: true)) {
            render(view: "edit", model: [foodDemandInstance: foodDemandInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'foodDemand.label', default: 'FoodDemand'), foodDemandInstance.food.name])

        redirect(controller: 'userProfileDashboard', action: 'index')
    }

    def delete(Long id) {
        def foodDemandInstance = FoodDemand.get(id)
        if (!foodDemandInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'foodDemand.label', default: 'FoodDemand'), id])
            redirect(controller: 'userProfileDashboard', action: 'index')
            return
        }

        try {
            foodDemandInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'foodDemand.label', default: 'FoodDemand'), id])
            redirect(controller: 'userProfileDashboard', action: 'index')
        } catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'foodDemand.label', default: 'FoodDemand'), id])

            redirect(controller: 'userProfileDashboard', action: 'index')
        }
    }
}
