package urbanfarmtrading

import grails.converters.JSON

class InfoController {

    def index() {
        [latLongFormat: UserProfileDashboardController.LAT_LON_FORMAT]
    }

    def listUserProfilesByLatLngBounds(double northEastLatitude, double northEastLongitude,
                                       double southWestLatitude, double southWestLongitude) {
        def hql = 'select s from UserProfile as s where s.active = true and s.hasOffers = true '
        hql += " and s.latitude <= :northEastLatitude and s.latitude >= :southWestLatitude "
        hql += " and s.longitude <= :northEastLongitude and s.longitude >= :southWestLongitude "
        hql += " and s.latitude <> 0 and s.longitude <> 0 "
        hql += " order by s.rating desc "

        def hqlParams = [:]
        hqlParams.put 'northEastLatitude', northEastLatitude
        hqlParams.put 'northEastLongitude', northEastLongitude
        hqlParams.put 'southWestLatitude', southWestLatitude
        hqlParams.put 'southWestLongitude', southWestLongitude

        def usersNearby = UserProfile.executeQuery(hql, hqlParams, [max: 30]).collect {
            [id: it.id, name: it.name, latitude: UserProfileDashboardController.LAT_LON_FORMAT.format(it.latitude), longitude: UserProfileDashboardController.LAT_LON_FORMAT.format(it.longitude), url: createLink(controller: 'userProfile', action: 'show', id: it.id)]
        }

        def result = [error: 0, payload: usersNearby]
        render result as JSON
    }
}
