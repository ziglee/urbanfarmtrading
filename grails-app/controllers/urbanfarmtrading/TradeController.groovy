package urbanfarmtrading

import grails.converters.JSON
import grails.transaction.Transactional
import org.joda.time.DateTime
import org.springframework.web.servlet.support.RequestContextUtils

import java.text.NumberFormat

@Transactional(readOnly = true)
class TradeController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", agree: "POST", sent: "POST",
            received: "POST", deleteFoodPortionTrade: "POST", approve: "POST", dontApprove: "POST", cancel: "POST"]

    def mailerService

    def index() {
        redirect(action: 'list', params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)

        def hqlParams = [:]
        def hql = 'select s from Trade as s where (s.engagedUser.id = :userProfileId or s.engagingUser.id = :userProfileId) '
        hqlParams.put 'userProfileId', session.userProfile.id

        if (params.sort)
            hql += " order by ${params.sort} "
        else
            hql += " order by s.lastUpdated "

        if (params.order)
            hql += params.order
        else
            hql += ' desc'

        def list = Trade.executeQuery(hql, hqlParams, params)
        def count = Trade.executeQuery(hql, hqlParams).size()

        [tradeInstanceList: list, tradeInstanceTotal: count]
    }

    def show(Long id) {
        def tradeInstance = Trade.get(id)

        if (tradeInstance.engagedUser.id != session.userProfile.id && tradeInstance.engagingUser.id != session.userProfile.id) {
            redirect(controller: 'userProfileDashboard')
            return
        }

        def fromEngaged = FoodPortionTrade.executeQuery('select s from FoodPortionTrade as s where s.fromEngaging = false and s.trade.id = :trade', [trade: tradeInstance.id])
        def fromEngaging = FoodPortionTrade.executeQuery('select s from FoodPortionTrade as s where s.fromEngaging = true and s.trade.id = :trade', [trade: tradeInstance.id])

        def otherParty = session.userProfile.id == tradeInstance.engagedUser.id ? tradeInstance.engagingUser : tradeInstance.engagedUser

        [tradeInstance: tradeInstance, fromEngaged: fromEngaged, fromEngaging: fromEngaging, otherParty: otherParty]
    }

    def create() {
        def userProfile = UserProfile.get(params.engagedUser.id)

        def now = new DateTime().withTimeAtStartOfDay().toDate()

        def hisFoodOffers = FoodOffer.executeQuery('select s from FoodOffer as s where s.availableUntil >= :now and s.userProfile.id = :userProfileId order by s.food.name asc', [userProfileId: userProfile.id, now: now])

        def mineFoodOffers = FoodOffer.executeQuery('select s from FoodOffer as s where s.availableUntil >= :now and s.userProfile.id = :userProfileId order by s.food.name asc', [userProfileId: session.userProfile.id, now: now])

        [userProfileInstance: userProfile, hisFoodOffers: hisFoodOffers, mineFoodOffers: mineFoodOffers]
    }

    @Transactional
    def save() {
        def json = request.JSON

        def engagedUser = UserProfile.get(json.engagedUser)
        def engagingUser = UserProfile.get(json.engagingUser)

        def trade = new Trade(engagingUser: engagingUser, engagedUser: engagedUser)

        if (!trade.save(flush: true)) {
            def result = [error: 1, payload: 'Erro ao criar proposta']
            render result as JSON
            return
        }

        def locale = RequestContextUtils.getLocale(request)
        def nf = NumberFormat.getNumberInstance(locale)

        json.fromEngaged.each {
            new FoodPortionTrade(food: Food.get(it.food), kilograms: nf.parse(it.kilograms),
                    trade: trade, fromEngaging: false).save(flush: true)
        };
        json.fromEngaging.each {
            new FoodPortionTrade(food: Food.get(it.food), kilograms: nf.parse(it.kilograms),
                    trade: trade, fromEngaging: true).save(flush: true)
        };

        def serverUrl = grailsApplication.config.grails.serverURL
        if (!serverUrl)
            serverUrl = 'http://localhost:8080'

        def showLink = serverUrl + createLink([controller: 'trade', action: 'show', id: trade.id])

        Thread.start {
            mailerService.tradeStarted(engagingUser.email, engagingUser.name, engagedUser.name, showLink)
        }

        def result = [error: 0, payload: createLink(action: 'show', id: trade.id)]
        render result as JSON
    }

    def edit(Long id) {
        def tradeInstance = Trade.get(id)

        if (tradeInstance.engagedUser.id != session.userProfile.id && tradeInstance.engagingUser.id != session.userProfile.id) {
            redirect(controller: 'userProfileDashboard')
            return
        }

        def now = new DateTime().withTimeAtStartOfDay().toDate()

        def engagedFoodOffers = FoodOffer.executeQuery('select s from FoodOffer as s where s.availableUntil >= :now and s.userProfile.id = :userProfileId order by s.food.name asc', ['userProfileId':tradeInstance.engagedUser.id, now: now])

        def engagingFoodOffers = FoodOffer.executeQuery('select s from FoodOffer as s where s.availableUntil >= :now and s.userProfile.id = :userProfileId order by s.food.name asc', ['userProfileId':tradeInstance.engagingUser.id, now: now])

        def engagedFoodPortionTrades = FoodPortionTrade.executeQuery('select s from FoodPortionTrade as s where s.fromEngaging = false and s.trade.id = :trade', [trade: tradeInstance.id])

        def engagingFoodPortionTrades = FoodPortionTrade.executeQuery('select s from FoodPortionTrade as s where s.fromEngaging = true and s.trade.id = :trade', [trade: tradeInstance.id])

        [tradeInstance: tradeInstance, engagingFoodOffers: engagingFoodOffers, engagedFoodOffers: engagedFoodOffers,
                engagedFoodPortionTrades: engagedFoodPortionTrades, engagingFoodPortionTrades: engagingFoodPortionTrades]
    }

    @Transactional
    def update() {
        def json = request.JSON

        def trade = Trade.get(json.id)

        if (trade.engagedUser.id != session.userProfile.id && trade.engagingUser.id != session.userProfile.id) {
            redirect(controller: 'userProfileDashboard')
            return
        }

        def locale = RequestContextUtils.getLocale(request)
        def nf = NumberFormat.getNumberInstance(locale)

        json.fromEngaged.each {
            if (it.id) {
                def f = FoodPortionTrade.get(it.id)
                f.kilograms = nf.parse(it.kilograms)
                f.save(flush: true)
            } else
                new FoodPortionTrade(food: Food.get(it.food), kilograms: nf.parse(it.kilograms),
                        trade: trade, fromEngaging: false).save(flush: true)
        };
        json.fromEngaging.each {
            if (it.id) {
                def f = FoodPortionTrade.get(it.id)
                f.kilograms = nf.parse(it.kilograms)
                f.save(flush: true)
            } else
                new FoodPortionTrade(food: Food.get(it.food), kilograms: nf.parse(it.kilograms),
                        trade: trade, fromEngaging: true).save(flush: true)
        };

        trade.engagedAgreed = false
        trade.engagingAgreed = false

        if (trade.engagedUser.id == session.userProfile.id)
            trade.engagedAgreed = true
        else
            trade.engagingAgreed = true

        trade.save(flush: true)

        sendTradeEmail(trade, false)

        def result = [error: 0, payload: createLink(action: 'show', id: trade.id)]
        render result as JSON
    }

    @Transactional
    def agree(Long id) {
        def tradeInstance = Trade.get(id)

        if (tradeInstance.engagedUser.id != session.userProfile.id
                && tradeInstance.engagingUser.id != session.userProfile.id) {
            redirect(controller: 'userProfileDashboard')
            return
        }

        if (tradeInstance.engagedUser.id == session.userProfile.id)
            tradeInstance.engagedAgreed = true
        else
            tradeInstance.engagingAgreed = true

        if (tradeInstance.engagedAgreed && tradeInstance.engagingAgreed)
            tradeInstance.status = TradeStatus.WAITING_SENDINGS

        tradeInstance.save(flush: true)

        sendTradeEmail(tradeInstance, false)

        redirect(action: 'show', id: id)
    }

    @Transactional
    def sent(Long id) {
        def tradeInstance = Trade.get(id)

        if (tradeInstance.engagedUser.id != session.userProfile.id
                && tradeInstance.engagingUser.id != session.userProfile.id) {
            redirect(controller: 'userProfileDashboard')
            return
        }

        if (tradeInstance.engagedUser.id == session.userProfile.id)
            tradeInstance.engagedSent = true
        else
            tradeInstance.engagingSent = true

        if (tradeInstance.engagedSent && tradeInstance.engagingSent)
            tradeInstance.status = TradeStatus.WAITING_ARRIVALS

        tradeInstance.save(flush: true)

        sendTradeEmail(tradeInstance, false)

        redirect(action: 'show', id: id)
    }

    @Transactional
    def received(Long id) {
        def tradeInstance = Trade.get(id)

        if (tradeInstance.engagedUser.id != session.userProfile.id
                && tradeInstance.engagingUser.id != session.userProfile.id) {
            redirect(controller: 'userProfileDashboard')
            return
        }

        if (tradeInstance.engagedUser.id == session.userProfile.id)
            tradeInstance.engagedReceived = true
        else
            tradeInstance.engagingReceived = true

        if (tradeInstance.engagedReceived && tradeInstance.engagingReceived)
            tradeInstance.status = TradeStatus.WAITING_REVIEWS

        tradeInstance.save(flush: true)

        sendTradeEmail(tradeInstance, false)

        redirect(action: 'show', id: id)
    }

    @Transactional
    def approve(Long id) {
        def tradeInstance = Trade.get(id)

        if (tradeInstance.engagedUser.id != session.userProfile.id
                && tradeInstance.engagingUser.id != session.userProfile.id) {
            redirect(controller: 'userProfileDashboard')
            return
        }

        if (tradeInstance.engagedUser.id == session.userProfile.id) {
            tradeInstance.engagedReviewed = true
            tradeInstance.engagedPositiveReview = true
        } else {
            tradeInstance.engagingReviewed = true
            tradeInstance.engagingPositiveReview = true
        }

        if (tradeInstance.engagedReviewed && tradeInstance.engagingReviewed)
            if (tradeInstance.engagedPositiveReview && tradeInstance.engagingPositiveReview)
                tradeInstance.status = TradeStatus.SUCCESS
            else
                tradeInstance.status = TradeStatus.FAILURE

        tradeInstance.save(flush: true)

        sendTradeEmail(tradeInstance, false)

        redirect(action: 'show', id: id)
    }

    @Transactional
    def dontApprove(Long id) {
        def tradeInstance = Trade.get(id)

        if (tradeInstance.engagedUser.id != session.userProfile.id
                && tradeInstance.engagingUser.id != session.userProfile.id) {
            redirect(controller: 'userProfileDashboard')
            return
        }

        if (tradeInstance.engagedUser.id == session.userProfile.id) {
            tradeInstance.engagedReviewed = true
            tradeInstance.engagedPositiveReview = false
        } else {
            tradeInstance.engagingReviewed = true
            tradeInstance.engagingPositiveReview = false
        }

        if (tradeInstance.engagedReviewed && tradeInstance.engagingReviewed)
            if (tradeInstance.engagedPositiveReview && tradeInstance.engagingPositiveReview)
                tradeInstance.status = TradeStatus.SUCCESS
            else
                tradeInstance.status = TradeStatus.FAILURE

        tradeInstance.save(flush: true)

        sendTradeEmail(tradeInstance, false)

        redirect(action: 'show', id: id)
    }

    @Transactional
    def cancel(Long id) {
        def tradeInstance = Trade.get(id)

        if (tradeInstance.engagedUser.id != session.userProfile.id
                && tradeInstance.engagingUser.id != session.userProfile.id) {
            redirect(controller: 'userProfileDashboard')
            return
        }

        tradeInstance.status = TradeStatus.FAILURE
        tradeInstance.save(flush: true)

        sendTradeEmail(tradeInstance, false)

        redirect(action: 'show', id: id)
    }

    @Transactional
    def delete(Long id) {
        def tradeInstance = Trade.get(id)

        if (tradeInstance.engagedUser.id != session.userProfile.id
                && tradeInstance.engagingUser.id != session.userProfile.id) {
            redirect(controller: 'userProfileDashboard')
            return
        }

        sendTradeEmail(tradeInstance, true)

        FoodPortionTrade.findAllByTrade(tradeInstance).each { it.delete(flush: true) }
        tradeInstance.delete(flush: true)

        redirect(action: 'list')
    }

    @Transactional
    def deleteFoodPortionTrade(Long id) {
        def foodPortionTrade = FoodPortionTrade.get(id)

        if (foodPortionTrade.trade.engagedUser.id != session.userProfile.id
                && foodPortionTrade.trade.engagingUser.id != session.userProfile.id) {
            def result = [error: 1, payload: 'Acesso negado']
            render result as JSON
            return
        }

        foodPortionTrade.delete(flush: true)

        def result = [error: 0, payload: '']
        render result as JSON
    }

    private void sendTradeEmail(Trade trade, boolean isRemoving) {
        def engagingUser = trade.engagingUser
        def engagedUser = trade.engagedUser

        def loggedIsEngaging = session.userProfile.id == trade.engagingUser.id

        def serverUrl = grailsApplication.config.grails.serverURL
        if (!serverUrl)
            serverUrl = 'http://localhost:8080'

        def showLink = serverUrl + createLink([controller: 'trade', action: 'show', id: trade.id])

        Thread.start {
            if (isRemoving)
                mailerService.tradeUpdated(
                        loggedIsEngaging ? engagingUser.email : engagedUser.email,
                        loggedIsEngaging ? engagingUser.name : engagedUser.name,
                        loggedIsEngaging ? engagedUser.name : engagingUser.name, showLink)
            else
                mailerService.tradeRemoved(
                        loggedIsEngaging ? engagingUser.email : engagedUser.email,
                        loggedIsEngaging ? engagingUser.name : engagedUser.name,
                        loggedIsEngaging ? engagedUser.name : engagingUser.name, showLink)
        }
    }
}
