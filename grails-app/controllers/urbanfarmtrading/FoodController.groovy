package urbanfarmtrading

import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class FoodController {

    def listFoodByTypeJson(Long id) {
        def type = params.type as FoodType

        def foods = Food.findAllByType(type).collect {
            [id: it.id, name: it.name, imgSrcBig: it.imgSrcBig, imgSrcThumbCropped: it.imgSrcThumbCropped]
        }

        def result = [error: 0, payload: foods]
        render result as JSON
    }
}
