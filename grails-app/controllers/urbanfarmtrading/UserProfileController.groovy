package urbanfarmtrading

import grails.transaction.Transactional
import org.joda.time.DateTime
import java.text.NumberFormat

@Transactional(readOnly = true)
class UserProfileController {

    static allowedMethods = [update: "POST"]

    private static final LAT_LON_FORMAT = NumberFormat.getNumberInstance(new Locale('en'))

    def index() { }

    def show(Long id) {
        def userProfile = UserProfile.get(id)

        def foodOffers = []
        def foodDemands = []
        def trades = []

        if (session.userProfile.id != userProfile.id) {
            foodOffers = FoodOffer.executeQuery('select s from FoodOffer as s where s.availableUntil >= :now and s.userProfile.id = :userProfileId order by s.food.name asc', [userProfileId: userProfile.id, now: new DateTime().withTimeAtStartOfDay().toDate()])
            foodDemands = FoodDemand.executeQuery('select s from FoodDemand as s where s.userProfile.id = :userProfileId order by s.food.name asc', [userProfileId: userProfile.id])
            trades = Trade.executeQuery('select t from Trade as t where (t.engagedUser.id = :myself and t.engagingUser.id = :him) or (t.engagedUser.id = :him and t.engagingUser.id = :myself)', [myself: session.userProfile.id, him: userProfile.id])
        }

        [userProfileInstance: userProfile, foodOffers: foodOffers, foodDemands: foodDemands, trades: trades]
    }

    def edit(Long id) {
        def userProfileInstance = UserProfile.get(id)
        if (!userProfileInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'userProfile.label', default: 'UserProfile'), id])
            redirect(action: "list")
            return
        }

        [userProfileInstance: userProfileInstance, latLongFormat: LAT_LON_FORMAT]
    }

    @Transactional
    def update(Long id, Long version) {
        def userProfileInstance = UserProfile.get(id)
        if (!userProfileInstance || userProfileInstance.id != session.userProfile.id) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'userProfile.label', default: 'UserProfile'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (userProfileInstance.version > version) {
                userProfileInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'userProfile.label', default: 'UserProfile')] as Object[],
                        "Another user has updated this UserProfile while you were editing")
                render(view: "edit", model: [userProfileInstance: userProfileInstance])
                return
            }
        }

        userProfileInstance.name = params.name
        userProfileInstance.latitude = params.double('latitude')
        userProfileInstance.longitude = params.double('longitude')
        userProfileInstance.address = params.address

        if (!userProfileInstance.save(flush: true)) {
            render(view: "edit", model: [userProfileInstance: userProfileInstance])
            return
        }

        session.userProfile.name = userProfileInstance.name
        session.userProfile.latitude = userProfileInstance.latitude
        session.userProfile.longitude = userProfileInstance.longitude

        flash.message = message(code: 'default.updated.message', args: [message(code: 'userProfile.label', default: 'UserProfile'), userProfileInstance.id])
        redirect(action: "show", id: userProfileInstance.id)
    }
}
