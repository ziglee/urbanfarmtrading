package urbanfarmtrading

import grails.transaction.Transactional
import org.springframework.dao.DataIntegrityViolationException
import urbanfarmtrading.util.MyNumberParseUtils

@Transactional(readOnly = true)
class FoodOfferController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def create() {
        [foodOfferInstance: new FoodOffer(params)]
    }

    @Transactional
    def save() {
        def foodOfferInstance = new FoodOffer(params)
        foodOfferInstance.userProfile = UserProfile.get(session.userProfile.id)

        if (foodOfferInstance.kilograms <= 0) {
            flash.message = 'O peso deve ser maior que zero'
            render(view: "create", model: [foodOfferInstance: foodOfferInstance])
            return
        }

        if (!foodOfferInstance.save(flush: true)) {
            render(view: "create", model: [foodOfferInstance: foodOfferInstance])
            return
        }

        if (!foodOfferInstance.userProfile.hasOffers) {
            foodOfferInstance.userProfile.hasOffers = true
            foodOfferInstance.userProfile.save(flush: true)
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'foodOffer.label', default: 'FoodOffer'), foodOfferInstance.food.name])

        redirect(controller: 'userProfileDashboard', action: 'index')
    }

    def edit(Long id) {
        def foodOfferInstance = FoodOffer.get(id)
        if (!foodOfferInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'foodOffer.label', default: 'FoodOffer'), id])
            redirect(controller: 'userProfileDashboard', action: 'index')
            return
        }

        [foodOfferInstance: foodOfferInstance]
    }

    @Transactional
    def update(Long id, Long version) {
        def foodOfferInstance = FoodOffer.get(id)
        if (!foodOfferInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'foodOffer.label', default: 'FoodOffer'), id])
            redirect(controller: 'userProfileDashboard', action: 'index')
            return
        }

        if (version != null) {
            if (foodOfferInstance.version > version) {
                foodOfferInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'foodOffer.label', default: 'FoodOffer')] as Object[],
                        "Another user has updated this FoodOffer while you were editing")
                render(view: "edit", model: [foodOfferInstance: foodOfferInstance])
                return
            }
        }

        foodOfferInstance.properties = params

        if (!foodOfferInstance.save(flush: true)) {
            render(view: "edit", model: [foodOfferInstance: foodOfferInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'foodOffer.label', default: 'FoodOffer'), foodOfferInstance.food.name])

        redirect(controller: 'userProfileDashboard', action: 'index')
    }

    def delete(Long id) {
        def foodOfferInstance = FoodOffer.get(id)
        if (!foodOfferInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'foodOffer.label', default: 'FoodOffer'), id])
            redirect(controller: 'userProfileDashboard', action: 'index')
            return
        }

        try {
            def userProfileId = foodOfferInstance.userProfile.id
            foodOfferInstance.delete(flush: true)
            UserProfile.get(userProfileId).updateHasOffers()

            flash.message = message(code: 'default.deleted.message', args: [message(code: 'foodOffer.label', default: 'FoodOffer'), id])
            redirect(controller: 'userProfileDashboard', action: 'index')
        } catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'foodOffer.label', default: 'FoodOffer'), id])

            redirect(controller: 'userProfileDashboard', action: 'index')
        }
    }
}
