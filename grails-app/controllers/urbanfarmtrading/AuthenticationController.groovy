package urbanfarmtrading

import grails.transaction.Transactional

@Transactional(readOnly = true)
class AuthenticationController {

    static allowedMethods = [authenticate: "POST", sendChangePasswordLink: "POST"]

    def mailerService
    def recaptchaService

    def index() {
    }

    def login() {

    }

    def logout() {
        session.invalidate()
        redirect(action: 'index')
    }

    def authenticate() {
        session.userProfile = null

        def userProfile = UserProfile.findByEmailAndActive(params.email, true)
        if (userProfile) {
            if (params.password.encodeAsSHA256().equals(userProfile.password)) {
                session.userProfile = [id: userProfile.id, name: userProfile.name, email: userProfile.email,
                        latitude: userProfile.latitude, longitude: userProfile.longitude]
                redirect(controller: 'userProfileDashboard', action: 'index')
                return
            }
        }

        flash.message = "Ops... Seu usuário ou senha são inválidos."
        redirect(action: 'login')
    }

    def create() {
        [userProfileInstance: new UserProfile(params)]
    }

    @Transactional
    def save() {
        def userProfileInstance = new UserProfile()

        userProfileInstance.name = params.name
        userProfileInstance.latitude = params.double('latitude')
        userProfileInstance.longitude = params.double('longitude')
        userProfileInstance.email = params.email
        userProfileInstance.address = params.address
        userProfileInstance.password = params.password

        if (!params.password.equals(params.repassword)) {
            flash.message = message(code: 'wrongPassConfirmation')
            render(view: "create", model: [userProfileInstance: userProfileInstance])
            return
        }

        if (!recaptchaService.verifyAnswer(session, request.getRemoteAddr(), params)) {
            flash.message = message(code: 'wrongCaptcha')
            render(view: "create", model: [userProfileInstance: userProfileInstance])
            return
        }

        userProfileInstance.passwordToHash()

        if (!userProfileInstance.save(flush: true)) {
            render(view: "create", model: [userProfileInstance: userProfileInstance])
            return
        }

        recaptchaService.cleanUp(session)

        def serverUrl = grailsApplication.config.grails.serverURL
        if (!serverUrl)
            serverUrl = 'http://localhost:8080'

        def activationLink = serverUrl + createLink([controller: 'authentication', action: 'validateEmailConfirmationCode', params: [email: userProfileInstance.email, activationCode: userProfileInstance.activationCode]])

        Thread.start {
            mailerService.sendEmailConfirmationCode(userProfileInstance, activationLink)
        }

        session.userProfile = [id: userProfileInstance.id, name: userProfileInstance.name, email: userProfileInstance.email, latitude: userProfileInstance.latitude, longitude: userProfileInstance.longitude]

        redirect(controller: 'userProfileDashboard', action: 'index')
    }

    @Transactional
    def validateEmailConfirmationCode() {
        if (!params.activationCode || !params.email) {
            redirect(action: 'index')
            return
        }

        def userProfileInstance = UserProfile.searchByEmailLowercase(params.email)
        if (userProfileInstance != null && userProfileInstance.activationCode != null && userProfileInstance.activationCode.equals(params.activationCode)) {
            userProfileInstance.activationCode = null
            userProfileInstance.emailConfirmed = true

            userProfileInstance.save(flush: true)

            flash.message = 'Email confirmado com sucesso'

            session.userProfile = [id: userProfileInstance.id, name: userProfileInstance.name, email: userProfileInstance.email, latitude: userProfileInstance.latitude, longitude: userProfileInstance.longitude]

            redirect(controller: 'userProfileDashboard', action: 'index')
        } else {
            flash.message = 'Código de confirmação inválido'
            redirect(action: 'index')
        }
    }

    def askForChangePasswordLink() {
    }

    @Transactional
    def sendChangePasswordLink() {
        def userProfileInstance = UserProfile.searchByEmailLowercase(params.email)
        if (userProfileInstance != null) {
            userProfileInstance.changePasswordCode = UUID.randomUUID().toString()
            userProfileInstance.save(flush: true)

            def serverUrl = grailsApplication.config.grails.serverURL
            if (!serverUrl)
                serverUrl = 'http://localhost:8080'

            def changePasswordLink = serverUrl + createLink([controller: 'authentication', action: 'changePasswordWithLink', params: [email: userProfileInstance.email, changePasswordCode: userProfileInstance.changePasswordCode]])

            Thread.start {
                mailerService.sendChangePasswordLink(userProfileInstance, changePasswordLink)
            }

            flash.message = "Um email lhe foi enviado com um link para que possa alterar sua senha"
        } else {
            flash.message = "Email inválido"
        }
        redirect(action: 'askForChangePasswordLink')
    }

    def changePasswordWithLink() {
        if (params.changePasswordCode == null || params.email == null) {
            redirect(action: 'index')
            return
        }

        def userProfileInstance = UserProfile.searchByEmailLowercase(params.email)
        if (userProfileInstance == null || userProfileInstance.changePasswordCode == null || !userProfileInstance.changePasswordCode.equals(params.changePasswordCode)) {
            flash.message = 'Solicitação de alteração de senha inválida'
            redirect(action: 'index')
            return
        }

        [userProfileInstance: userProfileInstance, changePasswordCode: params.changePasswordCode]
    }

    @Transactional
    def saveNewPassword() {
        if (!params.changePasswordCode) {
            redirect(action: 'index')
            return
        }

        def userProfileInstance = UserProfile.searchByEmailLowercase(params.email)
        if (userProfileInstance != null && userProfileInstance.changePasswordCode != null && userProfileInstance.changePasswordCode.equals(params.changePasswordCode) && params.password) {
            userProfileInstance.password = params.password
            userProfileInstance.changePasswordCode = null
            userProfileInstance.passwordToHash()

            if (!userProfileInstance.save(flush: true)) {
                render(view: "changePasswordWithLink", model: [userProfileInstance: userProfileInstance, changePasswordCode: params.changePasswordCode])
                return
            }

            flash.message = 'Senha alterada com sucesso'
            redirect(action: 'index')
            return
        }

        render(view: "changePasswordWithLink", model: [userProfileInstance: userProfileInstance, changePasswordCode: params.changePasswordCode])
    }

    def contact() { }

    def sendContact() {
        if (!recaptchaService.verifyAnswer(session, request.getRemoteAddr(), params)) {
            flash.message = 'Invalid code. Try again.'
            render(view: "contact")
        } else {
            recaptchaService.cleanUp(session)
            Thread.start {
                mailerService.sendContact(params.email, params.name, params.message)
            }
            flash.message = 'Your message has been sent. We will get in touch soon!'
            redirect(action: 'index')
        }
    }
}
