package urbanfarmtrading

class MailerService {

    def mailService

    def header = "<table width=\"600\" align=\"center\" style=\"border:1px solid #a0a0a0;font:normal 12px Tahoma,Arial;border-radius:4px\" cellspacing=\"10\"><tr><td style=\"background:#efefef;text-align:center\"><img width=\"100px\" src=\"http://urbanfarmtrading.org/static/images/organic.png\"/></td></tr><tr><td>";
    def footer = "</td></tr><tr><td style=\"background:#999;text-align:center;padding:10px;\"><a href=\"http://urbanfarmtrading.org\" style=\"text-decoration:none;color:#FFFFFF;font-weight:bold\">urbanfarmtrading.org</a></td></tr></table>";

    def sendEmailConfirmationCode(UserProfile userProfile, String activationLink) {
        mailService.sendMail {
            to userProfile.email
            subject "UrbanFarmTrading - Confirm your email"
            html header+"<h2>Welcome, ${userProfile.name}!</h2><p>You just registered at UrbanFarmTrading. To enjoy better the service, <b>confirm your email</b>.<br/><br/><a href='${activationLink}'>Click here to confirm your email.</a></p><p>Best regards,<br/>UrbanFarmTrading Team</p>"+footer
        }
    }

    def sendChangePasswordLink(UserProfile userProfile, String changePasswordLink) {
        mailService.sendMail {
            to userProfile.email
            subject "UrbanFarmTrading - Password change request"
            html header+"<h2>Hello, ${userProfile.name}!</h2><p>You requested a link to change you access password. <a href='${changePasswordLink}'>Click here to change your password.</a></p><p>Best regards,<br/>UrbanFarmTrading Team</p>"+footer
        }
    }

    def tradeUpdated(String userProfileEmail, String userProfileName, String otherPartName, String showUrl) {
        mailService.sendMail {
            to userProfileEmail
            subject "UrbanFarmTrading - Trade updated"
            html header+"<h2>Hello, ${userProfileName}!</h2><p>${otherPartName} updated a trade with you. <a href='${showUrl}'>Click here to see more details.</a></p><p>Best regards,<br/>UrbanFarmTrading Team</p>"+footer
        }
    }

    def tradeStarted(String userProfileEmail, String userProfileName, String otherPartName, String showUrl) {
        mailService.sendMail {
            to userProfileEmail
            subject "UrbanFarmTrading - You have a new trade"
            html header+"<h2>Hello, ${userProfileName}!</h2><p>${otherPartName} started a new trade with you. <a href='${showUrl}'>Click here to see more details.</a></p><p>Best regards,<br/>UrbanFarmTrading Team</p>"+footer
        }
    }

    def tradeRemoved(String userProfileEmail, String userProfileName, String otherPartName, String showUrl) {
        mailService.sendMail {
            to userProfileEmail
            subject "UrbanFarmTrading - A trade with you has been removed"
            html header+"<h2>Hello, ${userProfileName}!</h2><p>${otherPartName} removed a trade with you. <a href='${showUrl}'>Click here to see more details.</a></p><p>Best regards,<br/>UrbanFarmTrading Team</p>"+footer
        }
    }

    def sendContact(String emailc = '', String name = '', String message = '') {
        mailService.sendMail {
            to "cassio.landim@gmail.com"
            subject "Contato UFT"
            html "${header}<p>Nome: ${name}</p><p>E-mail: ${emailc}</p><p>Mensagem: ${message}</p>${footer}"
        }
    }
}
