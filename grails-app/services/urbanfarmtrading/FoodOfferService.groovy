package urbanfarmtrading

import org.joda.time.DateTime

class FoodOfferService {

    def List<UserProfile> whoFulfillDemand(UserProfile userProfile, double northEastLatitude, double northEastLongitude,
                                           double southWestLatitude, double southWestLongitude) {
        def foods = FoodDemand.executeQuery('select distinct(s.food.id) from FoodDemand as s where s.userProfile.id = :userProfileId',
                ['userProfileId':userProfile.id])

        if (!foods)
            return []

        def hql = 'select distinct(o.userProfile) from FoodOffer as o where o.availableUntil >= :now and o.userProfile.id != :userProfileId and o.food.id in (:foods) '
        hql += ' and o.userProfile.active = true and o.userProfile.hasOffers = true '
        hql += " and o.userProfile.latitude <= :northEastLatitude and o.userProfile.latitude >= :southWestLatitude "
        hql += " and o.userProfile.longitude <= :northEastLongitude and o.userProfile.longitude >= :southWestLongitude "

        def hqlParams = [:]
        hqlParams.put 'userProfileId', userProfile.id
        hqlParams.put 'now', new DateTime().withTimeAtStartOfDay().toDate()
        hqlParams.put 'foods', foods
        hqlParams.put 'northEastLatitude', northEastLatitude
        hqlParams.put 'northEastLongitude', northEastLongitude
        hqlParams.put 'southWestLatitude', southWestLatitude
        hqlParams.put 'southWestLongitude', southWestLongitude

        return UserProfile.executeQuery(hql, hqlParams, [max: 30])
    }
}
