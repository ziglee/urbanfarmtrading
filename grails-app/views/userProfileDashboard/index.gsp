<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoYE4hTspwbzPaQnkfix4OOo_ug-gDkzQ&sensor=false">
        </script>
        <script type="text/javascript">
            var markers = [];
            var map = null;
            var markersUrl = '${createLink(action: 'listUserProfilesByLatLngBounds')}';

            $(document).ready(function() {
                $('#only_who_fulfill').click(mapModeChanged);
            });

            function mapModeChanged() {
                if(this.checked) {
                    markersUrl = '${createLink(action: 'listUserProfilesWhoFulfillMyDemandByLatLngBounds')}';
                } else {
                    markersUrl = '${createLink(action: 'listUserProfilesByLatLngBounds')}';
                }
                mapBoundsChanged();
            }

            function mapBoundsChanged() {
                $.ajax({
                    url: markersUrl,
                    contentType: 'application/json',
                    dataType: 'json',
                    data: {
                        northEastLatitude: map.getBounds().getNorthEast().lat(),
                        northEastLongitude: map.getBounds().getNorthEast().lng(),
                        southWestLatitude: map.getBounds().getSouthWest().lat(),
                        southWestLongitude: map.getBounds().getSouthWest().lng()
                    },
                    success: function(doc) {
                        if (doc.error == 0) {
                            clearMarkers();
                            $.each(doc.payload, function(k, v) {
                                addMarker(map, v.latitude, v.longitude, v.name, v.url);
                            });
                        } else {
                            alert(doc.payload);
                        }
                    },
                    error: function() {
                        alert('<g:message code="error" />');
                    }
                });
            }

            function initializeMap() {
                var mapOptions = {
                    <g:if test="${params.latitude && params.longitude}">
                        center: new google.maps.LatLng(${latLongFormat.format(params.double('latitude'))}, ${latLongFormat.format(params.double('longitude'))}),
                    </g:if>
                    <g:else>
                        center: new google.maps.LatLng(${latLongFormat.format(session.userProfile.latitude)}, ${latLongFormat.format(session.userProfile.longitude)}),
                    </g:else>
                    zoom: 14
                };

                map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

                google.maps.event.addListener(map, 'bounds_changed', mapBoundsChanged);

                new google.maps.Marker({
                    position: new google.maps.LatLng(${latLongFormat.format(session.userProfile.latitude)}, ${latLongFormat.format(session.userProfile.longitude)}),
                    icon: '${resource(dir:'images', file:'map_pin_stroke.png')}',
                    map: map
                });
            }

            google.maps.event.addDomListener(window, 'load', initializeMap);

            function addMarker(map, latitude, longitude, title, url) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(latitude, longitude),
                    icon: '${resource(dir:'images', file:'map_pin_alt.png')}',
                    title: title,
                    map: map
                });

                google.maps.event.addListener(marker, 'click', function() {
                    window.location.href = url;
                });

                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            function clearMarkers() {
                setAllMap(null);
                markers = [];
            }
        </script>
    </head>
    <body>
        <g:if test="${flash.message}">
            <div class="alert alert-info alert-dismissable" role="status">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                ${flash.message}
            </div>
        </g:if>

        <div class="panel panel-default">
            <div class="panel-body">
                <div id="map-canvas" style="width: 100%; height: 500px"></div>
                <input type="checkbox" id="only_who_fulfill"/> <g:message code="showOnlyWhoFulfillMyDemands"/>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-transfer"></span> <g:message code="trade.withMe.plural.label" /></h3>
            </div>
            <table class="table table-hover">
                <g:if test="${trades}">
                    <g:each in="${trades}" var="trade">
                        <tr>
                            <td><img height="40px" src="${session.userProfile.id == trade.engagedUser.id ? trade.engagingUser.gravatarUrl : trade.engagedUser.gravatarUrl}" /></td>
                            <td><g:link controller="userProfile" action="show" id="${session.userProfile.id == trade.engagedUser.id ? trade.engagingUser.id : trade.engagedUser.id}">${session.userProfile.id == trade.engagedUser.id ? trade.engagingUser.name.encodeAsHTML() : trade.engagedUser.name.encodeAsHTML()}</g:link></td>
                            <td><g:message code="${trade.status.code}"/></td>
                            <td><g:formatDate date="${trade.lastUpdated}" format="yyyy-MM-dd HH:mm"/></td>
                            <td style="width: 1%;"><g:link controller="trade" action="show" id="${trade.id}" class="glyphicon glyphicon-search"/></td>
                        </tr>
                    </g:each>
                </g:if>
                <g:else>
                    <tr class="warning">
                        <td colspan="2"><g:message code="trade.zeroInProgress" /></td>
                    </tr>
                </g:else>
            </table>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="glyphicon glyphicon-cutlery"></span> <g:message code="iAmOffering" /></h3>
                    </div>
                    <table class="table table-hover">
                        <g:if test="${foodOffers}">
                            <g:each in="${foodOffers}" var="foodOffer">
                                <tr>
                                    <td style="width: 100%">${foodOffer.food.name.encodeAsHTML()}</td>
                                    <td style="text-align: right;">
                                        <span class="badge"><g:formatNumber number="${foodOffer.kilograms}" minFractionDigits="1"/> kg</span>
                                    </td>
                                    <td>
                                        <g:link class="glyphicon glyphicon-pencil" controller="foodOffer" action="edit" id="${foodOffer.id}"/>
                                    </td>
                                </tr>
                            </g:each>
                        </g:if>
                        <g:else>
                            <tr class="warning">
                                <td><g:message code="foodOffer.zeroOffers" /></td>
                            </tr>
                        </g:else>
                        <tr>
                            <td colspan="3">
                                <g:link class="btn btn-default btn-primary" controller="foodOffer" action="create"><span class="glyphicon glyphicon-plus"></span></g:link>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="glyphicon glyphicon-cutlery"></span> <g:message code="iAmRequesting" /></h3>
                    </div>
                    <table class="table table-hover">
                        <g:if test="${foodDemands}">
                            <g:each in="${foodDemands}" var="foodDemand">
                                <tr>
                                    <td  style="width: 100%">${foodDemand.food.name.encodeAsHTML()}</td>
                                    <td>
                                        <g:link class="glyphicon glyphicon-pencil" controller="foodDemand" action="edit" id="${foodDemand.id}"/>
                                </tr>
                            </g:each>
                        </g:if>
                        <g:else>
                            <tr class="warning">
                                <td><g:message code="foodDemand.zeroDemands" /></td>
                            </tr>
                        </g:else>
                        <tr>
                            <td colspan="2">
                                <g:link class="btn btn-default btn-primary" controller="foodDemand" action="create"><span class="glyphicon glyphicon-plus"></span></g:link>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>