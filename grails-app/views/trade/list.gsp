<%@ page import="urbanfarmtrading.Trade" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'trade.label', default: 'Trade')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
        <script src="${resource(dir: 'js', file: 'trade.js')}"></script>
	</head>
	<body>
		<g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>

        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <g:sortableColumn property="status" title="${message(code: 'trade.status.label', default: 'Status')}" />
                    <g:sortableColumn property="engagingUser" title="${message(code: 'trade.engagingUser.label', default: 'Engaging User')}" />
                    <g:sortableColumn property="engagedUser" title="${message(code: 'trade.engagedUser.label', default: 'Engaged User')}" />
                    <g:sortableColumn property="lastUpdated" title="${message(code: 'trade.lastUpdated.label')}" />
                </tr>
            </thead>
            <tbody>
            <g:each in="${tradeInstanceList}" status="i" var="tradeInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                    <td><g:link action="show" id="${tradeInstance.id}"><g:message code="${tradeInstance.status.code}"/></g:link></td>
                    <td>${tradeInstance.engagingUser.name.encodeAsHTML()}</td>
                    <td>${tradeInstance.engagedUser.name.encodeAsHTML()}</td>
                    <td><g:formatDate date="${tradeInstance.lastUpdated}" type="datetime" style="SHORT"/></td>
                </tr>
            </g:each>
            </tbody>
        </table>

        <div class="pagination">
            <g:paginate total="${tradeInstanceTotal}" />
        </div>
	</body>
</html>
