<%@ page import="urbanfarmtrading.Trade" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'trade.label', default: 'Trade')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
        <script src="${resource(dir: 'js', file: 'trade.js')}"></script>
        <script>
            $(document).ready(function() {
                $('#addHis').click(addHisClick);
                $('#addMine').click(addMineClick);
            });

            function addHisClick() {
                addFood('hisFoodOffersSelect', 'hisFoodOffers')
            }

            function addMineClick() {
                addFood('mineFoodOffersSelect', 'mineFoodOffers')
            }

            function addFood(fromSelectId, ulId) {
                var option = $('#'+ fromSelectId +' option:selected');
                var foodOfferId = option.attr('data-food-offer-id');
                var exists = false;

                $.each($('#'+ ulId +' li'), function(k, v) {
                    if ($(this).attr('data-food-offer-id') == foodOfferId) {
                        exists = true;
                    }
                });

                if (exists) {
                    alert('<g:message code="trade.alert.foodAlreadyAdded" />');
                    return;
                }

                var foodId = option.attr('data-food-id');
                var total = option.attr('data-kilograms');

                var li =  '<li ';
                li += " data-food-id='"+ foodId +"'";
                li += " data-food-offer-id='"+ foodOfferId +"'>";
                li += option.attr('data-food-name');
                li += " <div class='input-group'><input type='text' name='kilograms' class='amount form-control col-md-3' value='"+ total +"'/><span class='input-group-addon'>kg</span>";
                li += "<span class='input-group-btn'><input type='button' value='X' class='removeOffer btn btn-danger' /></span></div>";
                li += '</li>';

                $('#' + ulId).append(li);

                $('.removeOffer').click(removeOfferClick);
            }

            function removeOfferClick() {
                $(this).parents('li').remove();
            }

            function formSubmit() {
                var fromEngaged = eachFoodOfferIterate('hisFoodOffers');
                var fromEngaging = eachFoodOfferIterate('mineFoodOffers');

                var trade = {engagingUser: ${session.userProfile.id},
                    engagedUser: ${userProfileInstance.id},
                    fromEngaging: fromEngaging, fromEngaged: fromEngaged};

                $.ajax({
                    url: '${createLink(action: 'save')}',
                    contentType: 'application/json',
                    processData: false,
                    dataType: 'json',
                    type: 'POST',
                    data: JSON.stringify(trade),
                    success: function(doc) {
                        if (doc.error == 0) {
                            window.location.href = doc.payload;
                        } else {
                            alert(doc.payload);
                        }
                    },
                    error: function() {
                        alert('<g:message code="error" />');
                    }
                });
            }

            function eachFoodOfferIterate(ulId) {
                var arrayToAdd = [];

                $.each($('#'+ ulId +' li'), function(k, v) {
                    var foodId = $(this).attr('data-food-id');
                    var kilograms = $(this).find('input[name=kilograms]').val();
                    var foodPortionTrade = {food: foodId, kilograms: kilograms};
                    arrayToAdd.push(foodPortionTrade);
                });

                return arrayToAdd;
            }
        </script>
	</head>
	<body>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-info-sign"></span> <g:message code="default.create.label" args="[entityName]" /></h3>
            </div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt><g:message code="trade.engagedUser.label" default="Engaged User" /></dt>
                    <dd><g:link controller="userProfile" action="show" id="${userProfileInstance.id}">${userProfileInstance.name.encodeAsHTML()}</g:link></dd>
                </dl>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-transfer"></span> <g:message code="trade.composite" /></h3>
            </div>
            <table class="table">
                <tr>
                    <th><g:message code="trade.iGiveMy" /></th>
                    <th><g:message code="trade.forYour" /></th>
                </tr>
                <tr>
                    <td>
                        <select name="foodOffer.id" id="mineFoodOffersSelect" class="form-control">
                            <g:each in="${mineFoodOffers}" var="foodOffer">
                                <option value="${foodOffer.id}" data-food-name='${foodOffer.food.name.encodeAsHTML()}'
                                        data-kilograms='${formatNumber(number:foodOffer.kilograms, minFractionDigits: 1)}'
                                        data-food-id="${foodOffer.food.id}" data-food-offer-id="${foodOffer.id}">
                                    ${foodOffer.food.name.encodeAsHTML()} <g:formatNumber number="${foodOffer.kilograms}" minFractionDigits="1"/> kg
                                </option>
                            </g:each>
                        </select>
                    </td>
                    <td>
                        <select name="foodOffer.id" id="hisFoodOffersSelect" class="form-control">
                            <g:each in="${hisFoodOffers}" var="foodOffer">
                                <option value="${foodOffer.id}" data-food-name='${foodOffer.food.name.encodeAsHTML()}'
                                        data-kilograms='${formatNumber(number:foodOffer.kilograms, minFractionDigits: 1)}'
                                        data-food-id="${foodOffer.food.id}" data-food-offer-id="${foodOffer.id}">
                                    ${foodOffer.food.name.encodeAsHTML()} <g:formatNumber number="${foodOffer.kilograms}" minFractionDigits="1"/> kg
                                </option>
                            </g:each>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="button" value="+" id="addMine" class="btn btn-success"/>
                    </td>
                    <td>
                        <input type="button" value="+" id="addHis" class="btn btn-success"/>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-4"><ul id="mineFoodOffers"></ul></td>
                    <td class="col-md-4"><ul id="hisFoodOffers"></ul></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="button" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" onclick="formSubmit();"/>
                    </td>
                </tr>
            </table>
        </div>
	</body>
</html>
