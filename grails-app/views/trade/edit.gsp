<%@ page import="urbanfarmtrading.Trade" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
    <head>
        <meta name="layout" content="main">
        <g:set var="entityName" value="${message(code: 'trade.label', default: 'Trade')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
        <script src="${resource(dir: 'js', file: 'trade.js')}"></script>
        <script>
            $(document).ready(function() {
                $('#addEngaged').click(addEngagedClick);
                $('#addEngaging').click(addEngagingClick);
                $('.removeOffer').click(removeOfferClick);
            });

            function addEngagedClick() {
                addFood('engagedFoodOffersSelect', 'engagedFoodOffers')
            }

            function addEngagingClick() {
                addFood('engagingFoodOffersSelect', 'engagingFoodOffers')
            }

            function addFood(fromSelectId, ulId) {
                var option = $('#'+ fromSelectId +' option:selected');
                var foodOfferId = option.attr('data-food-offer-id');
                var foodId = option.attr('data-food-id');
                var exists = false;

                $.each($('#'+ ulId +' li'), function(k, v) {
                    if ($(this).attr('data-food-id') == foodId) {
                        exists = true;
                    }
                });

                if (exists) {
                    alert('<g:message code="trade.alert.foodAlreadyAdded" />');
                    return;
                }

                var total = option.attr('data-kilograms');

                var li = '<li ';
                li += " data-id='' data-food-id='"+ foodId +"'>";
                li += option.attr('data-food-name');
                li += " <div class='input-group'><input type='text' name='kilograms' class='amount form-control col-md-3' value='"+ total +"'/><span class='input-group-addon'>kg</span>";
                li += "<span class='input-group-btn'><input type='button' value='X' class='removeOffer btn btn-danger' /></span></div>";
                li += '</li>';

                $('#' + ulId).append(li);

                $('.removeOffer').click(removeOfferClick);
            }

            function removeOfferClick() {
                var li = $(this).parents('li');
                var id = li.attr('data-id');

                if (id != '') {
                    $.ajax({
                        url: '${createLink(action: 'deleteFoodPortionTrade')}',
                        type: 'POST',
                        data: {
                            id: id
                        },
                        success: function(doc) {
                            if (doc.error == 0) {
                                li.remove();
                            } else {
                                alert(doc.payload);
                            }
                        },
                        error: function() {
                            alert('<g:message code="error" />');
                        }
                    });
                } else {
                    li.remove();
                }
            }

            function formSubmit() {
                var fromEngaged = eachFoodOfferIterate('engagedFoodOffers');
                var fromEngaging = eachFoodOfferIterate('engagingFoodOffers');

                var trade = {id: ${tradeInstance.id}, fromEngaging: fromEngaging, fromEngaged: fromEngaged};

                $.ajax({
                    url: '${createLink(action: 'update')}',
                    contentType: 'application/json',
                    processData: false,
                    dataType: 'json',
                    type: 'POST',
                    data: JSON.stringify(trade),
                    success: function(doc) {
                        if (doc.error == 0) {
                            alert('Trade updated successfully.');
                        } else {
                            alert(doc.payload);
                        }
                    },
                    error: function() {
                        alert('<g:message code="error" />');
                    }
                });
            }

            function eachFoodOfferIterate(ulId) {
                var arrayToAdd = [];

                $.each($('#'+ ulId +' li'), function(k, v) {
                    var id = $(this).attr('data-id');
                    var foodId = $(this).attr('data-food-id');
                    var kilograms = $(this).find('input[name=kilograms]').val();
                    var foodPortionTrade = {id: id, food: foodId, kilograms: kilograms};
                    arrayToAdd.push(foodPortionTrade);
                });

                return arrayToAdd;
            }
        </script>
    </head>
    <body>
        <g:if test="${flash.message}">
            <div class="alert alert-info alert-dismissable" role="status">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                ${flash.message}
            </div>
        </g:if>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-transfer"></span> <g:message code="trade.adjust" /></h3>
            </div>
            <table class="table">
                <tr>
                    <th>${tradeInstance.engagingUser.name.encodeAsHTML()}</th>
                    <th>${tradeInstance.engagedUser.name.encodeAsHTML()}</th>
                </tr>
                <tr>
                    <td>
                        <select id="engagingFoodOffersSelect" class="form-control">
                            <g:each in="${engagingFoodOffers}" var="foodOffer">
                                <option value="${foodOffer.id}" data-food-name='${foodOffer.food.name.encodeAsHTML()}'
                                        data-kilograms='${formatNumber(number:foodOffer.kilograms, minFractionDigits: 1)}'
                                        data-food-id="${foodOffer.food.id}" >
                                    ${foodOffer.food.name.encodeAsHTML()} <g:formatNumber number="${foodOffer.kilograms}" minFractionDigits="1"/> kg
                                </option>
                            </g:each>
                        </select>
                    </td>
                    <td>
                        <select id="engagedFoodOffersSelect" class="form-control">
                            <g:each in="${engagedFoodOffers}" var="foodOffer">
                                <option value="${foodOffer.id}" data-food-name='${foodOffer.food.name.encodeAsHTML()}'
                                        data-kilograms='${formatNumber(number:foodOffer.kilograms, minFractionDigits: 1)}'
                                        data-food-id="${foodOffer.food.id}" >
                                    ${foodOffer.food.name.encodeAsHTML()} <g:formatNumber number="${foodOffer.kilograms}" minFractionDigits="1"/> kg
                                </option>
                            </g:each>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="button" value="+" id="addEngaging" class="btn btn-success"/>
                    </td>
                    <td>
                        <input type="button" value="+" id="addEngaged" class="btn btn-success"/>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-4">
                        <ul id="engagingFoodOffers">
                            <g:each in="${engagingFoodPortionTrades}" var="foodPortionTrade">
                                <li data-food-id='${foodPortionTrade.food.id}' data-id='${foodPortionTrade.id}'>
                                    ${foodPortionTrade.food.name.encodeAsHTML()}
                                    <div class='input-group'>
                                        <input type='text' name='kilograms' class='amount form-control col-md-3'
                                               value='${formatNumber(number: foodPortionTrade.kilograms, minFractionDigits: 1)}'/>
                                        <span class='input-group-addon'>kg</span>
                                        <span class='input-group-btn'>
                                            <input type='button' value='X' class='removeOffer btn btn-danger' />
                                        </span>
                                    </div>
                                </li>
                            </g:each>
                        </ul>
                    </td>
                    <td class="col-md-4">
                        <ul id="engagedFoodOffers">
                            <g:each in="${engagedFoodPortionTrades}" var="foodPortionTrade">
                                <li data-food-id='${foodPortionTrade.food.id}' data-id='${foodPortionTrade.id}'>
                                    ${foodPortionTrade.food.name.encodeAsHTML()}
                                    <div class='input-group'>
                                        <input type='text' name='kilograms' class='amount form-control col-md-3'
                                               value='${formatNumber(number: foodPortionTrade.kilograms, minFractionDigits: 1)}'/>
                                        <span class='input-group-addon'>kg</span>
                                        <span class='input-group-btn'>
                                            <input type='button' value='X' class='removeOffer btn btn-danger' />
                                        </span>
                                    </div>
                                </li>
                            </g:each>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <g:form method="post">
                            <g:hiddenField name="id" value="${tradeInstance.id}" />
                            <g:hiddenField name="version" value="${tradeInstance?.version}" />
                            <g:link action="show" id="${tradeInstance.id}" class="btn btn-info"><g:message code="back" /></g:link>
                            <input type="button" class="btn btn-primary" value="${message(code: 'default.button.update.label', default: 'Update')}" onclick="formSubmit();"/>
                            <g:actionSubmit class="btn btn-danger" action="delete"
                                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                        </g:form>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>