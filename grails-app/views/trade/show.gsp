<%@ page import="urbanfarmtrading.TradeStatus; urbanfarmtrading.UserProfile" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'trade.label', default: 'Trade')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
        <script src="${resource(dir: 'js', file: 'trade.js')}"></script>
	</head>
	<body>
        <g:if test="${flash.message}">
            <div class="alert alert-info alert-dismissable" role="status">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                ${flash.message}
            </div>
        </g:if>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-info-sign"></span> <g:message code="default.show.label" args="[entityName]" /></h3>
            </div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt><g:message code="trade.status.label" default="Status" /></dt>
                    <dd>
                        <g:if test="${tradeInstance.status == TradeStatus.SUCCESS || tradeInstance.status == TradeStatus.FAILURE}">
                            <g:if test="${tradeInstance.status == TradeStatus.SUCCESS}">
                                <span class="label label-success">
                                    <g:message code="${tradeInstance.status.code}"/>
                                </span>
                            </g:if>
                            <g:else>
                                <span class="label label-danger">
                                    <g:message code="${tradeInstance.status.code}"/>
                                </span>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:message code="${tradeInstance.status.code}"/>
                        </g:else>
                    </dd>
                    <dt><g:message code="trade.otherPart" /></dt>
                    <dd><g:link controller="userProfile" action="show" id="${otherParty.id}">${otherParty.name.encodeAsHTML()}</g:link></dd>
                    <g:if test="${tradeInstance.status != urbanfarmtrading.TradeStatus.WAITING_AGREEMENT}">
                        <dt><g:message code="userProfile.email.label" /></dt>
                        <dd><a href="mailto:${otherParty.email.encodeAsHTML()}">${otherParty.email.encodeAsHTML()}</a></dd>
                        <dt><g:message code="userProfile.address.label" /></dt>
                        <dd>${otherParty.address.encodeAsHTML()}</dd>
                    </g:if>
                </dl>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-transfer"></span> <g:message code="trade.composite" /></h3>
            </div>
            <table class="table">
                <tr>
                    <th>
                        <p class="text-center">
                            <g:link controller="userProfile" action="show" id="${tradeInstance.engagingUser.id}">${tradeInstance.engagingUser.name.encodeAsHTML()}</g:link>
                        </p>
                    </th>
                    <th>
                        <p class="text-center">
                            <g:link controller="userProfile" action="show" id="${tradeInstance.engagedUser.id}">${tradeInstance.engagedUser.name.encodeAsHTML()}</g:link>
                        </p>
                    </th>
                </tr>
                <tr>
                    <td>
                        <ul class="list-group">
                            <g:each in="${fromEngaging}" var="foodPortionTrade">
                                <li class="list-group-item">
                                    <span class="badge"><g:formatNumber number="${foodPortionTrade.kilograms}" minFractionDigits="1"/> kg</span>
                                    ${foodPortionTrade.food.name.encodeAsHTML()}
                                </li>
                            </g:each>
                        </ul>
                    </td>
                    <td>
                        <ul class="list-group">
                            <g:each in="${fromEngaged}" var="foodPortionTrade">
                                <li class="list-group-item">
                                    <span class="badge"><g:formatNumber number="${foodPortionTrade.kilograms}" minFractionDigits="1"/> kg</span>
                                    ${foodPortionTrade.food.name.encodeAsHTML()}
                                </li>
                            </g:each>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <g:form>
                            <fieldset class="buttons">
                                <g:hiddenField name="id" value="${tradeInstance?.id}" />
                                <g:if test="${tradeInstance.status == TradeStatus.WAITING_AGREEMENT}">
                                    <g:if test="${(!tradeInstance.engagedAgreed && tradeInstance.engagedUser.id == session.userProfile.id) || (!tradeInstance.engagingAgreed && tradeInstance.engagingUser.id == session.userProfile.id)}">
                                        <g:actionSubmit class="btn btn-primary" action="agree" value="${message(code: 'trade.button.iAgree')}" />
                                    </g:if>
                                    <g:link class="btn btn-primary" action="edit" id="${tradeInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                                    <g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                                </g:if>
                                <g:elseif test="${tradeInstance.status == TradeStatus.WAITING_SENDINGS}">
                                    <g:if test="${(!tradeInstance.engagedSent && tradeInstance.engagedUser.id == session.userProfile.id) || (!tradeInstance.engagingSent && tradeInstance.engagingUser.id == session.userProfile.id)}">
                                        <g:actionSubmit class="btn btn-primary" action="sent" value="${message(code: 'trade.button.iShipped')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                                    </g:if>
                                </g:elseif>
                                <g:elseif test="${tradeInstance.status == TradeStatus.WAITING_ARRIVALS}">
                                    <g:if test="${(!tradeInstance.engagedReceived && tradeInstance.engagedUser.id == session.userProfile.id) || (!tradeInstance.engagingReceived && tradeInstance.engagingUser.id == session.userProfile.id)}">
                                        <g:actionSubmit class="btn btn-primary" action="received" value="${message(code: 'trade.button.iReceived')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                                    </g:if>
                                </g:elseif>
                                <g:elseif test="${tradeInstance.status == TradeStatus.WAITING_REVIEWS}">
                                    <g:if test="${(!tradeInstance.engagedReviewed && tradeInstance.engagedUser.id == session.userProfile.id) || (!tradeInstance.engagingReviewed && tradeInstance.engagingUser.id == session.userProfile.id)}">
                                        <g:actionSubmit class="btn btn-primary" action="approve" value="${message(code: 'trade.button.iApprove')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                                        <g:actionSubmit class="btn btn-primary" action="dontApprove" value="${message(code: 'trade.button.iDontApprove')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                                    </g:if>
                                </g:elseif>

                                <g:if test="${tradeInstance.status != TradeStatus.WAITING_AGREEMENT && tradeInstance.status != TradeStatus.FAILURE && tradeInstance.status != TradeStatus.SUCCESS}">
                                    <g:actionSubmit class="btn btn-danger" action="cancel" value="${message(code: 'trade.button.cancel')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                                </g:if>
                            </fieldset>
                        </g:form>
                    </td>
                </tr>
            </table>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span class="glyphicon glyphicon-time"></span> <g:message code="trade.flow"/>
                </h3>
            </div>
            <table class="table">
                <tr>
                    <th>Who</th>
                    <th>Agreed</th>
                    <th>Shipped</th>
                    <th>Received</th>
                    <th>Review</th>
                </tr>
                <tr>
                    <td>${tradeInstance.engagingUser.name}</td>
                    <td>
                        <g:if test="${tradeInstance.engagingAgreed}">
                            <span class="flow-btn btn btn-success" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.agreed', args: [tradeInstance.engagingUser.name])}"><span class="glyphicon glyphicon-ok"></span></span>
                        </g:if>
                        <g:else>
                            <span class="flow-btn btn btn-warning" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.waitingAgreement', args: [tradeInstance.engagingUser.name])}"><span class="glyphicon glyphicon-time"></span></span>
                        </g:else>
                    </td>
                    <td>
                        <g:if test="${tradeInstance.engagingSent}">
                            <span class="flow-btn btn btn-success" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.shipped', args: [tradeInstance.engagingUser.name])}"><span class="glyphicon glyphicon-ok"></span></span>
                        </g:if>
                        <g:else>
                            <span class="flow-btn btn btn-warning" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.hasNotShippedYet', args: [tradeInstance.engagingUser.name])}"><span class="glyphicon glyphicon-time"></span></span>
                        </g:else>
                    </td>
                    <td>
                        <g:if test="${tradeInstance.engagingReceived}">
                            <span class="flow-btn btn btn-success" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.received', args: [tradeInstance.engagingUser.name])}"><span class="glyphicon glyphicon-ok"></span></span>
                        </g:if>
                        <g:else>
                            <span class="flow-btn btn btn-warning" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.hasNotReceivedYet', args: [tradeInstance.engagingUser.name])}"><span class="glyphicon glyphicon-time"></span></span>
                        </g:else>
                    </td>
                    <td>
                        <g:if test="${tradeInstance.engagingReviewed}">
                            <g:if test="${tradeInstance.engagingPositiveReview}">
                                <span class="flow-btn btn btn-success" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.approved', args: [tradeInstance.engagingUser.name])}"><span class="glyphicon glyphicon-thumbs-up"></span></span>
                            </g:if>
                            <g:else>
                                <span class="flow-btn btn btn-danger" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.hasNotApproved', args: [tradeInstance.engagingUser.name])}"><span class="glyphicon glyphicon-thumbs-down"></span></span>
                            </g:else>
                        </g:if>
                        <g:else>
                            <span class="flow-btn btn btn-warning" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.hasNotReviewedYet', args: [tradeInstance.engagingUser.name])}"><span class="glyphicon glyphicon-time"></span></span>
                        </g:else>
                    </td>
                </tr>
                <tr>
                    <td>${tradeInstance.engagedUser.name}</td>
                    <td>
                        <g:if test="${tradeInstance.engagedAgreed}">
                            <span class="flow-btn btn btn-success" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.agreed', args: [tradeInstance.engagedUser.name])}"><span class="glyphicon glyphicon-ok"></span></span>
                        </g:if>
                        <g:else>
                            <span class="flow-btn btn btn-warning" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.waitingAgreement', args: [tradeInstance.engagedUser.name])}"><span class="glyphicon glyphicon-time"></span></span>
                        </g:else>
                    </td>
                    <td>
                        <g:if test="${tradeInstance.engagedSent}">
                            <span class="flow-btn btn btn-success" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.shipped', args: [tradeInstance.engagedUser.name])}"><span class="glyphicon glyphicon-ok"></span></span>
                        </g:if>
                        <g:else>
                            <span class="flow-btn btn btn-warning" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.hasNotShippedYet', args: [tradeInstance.engagedUser.name])}"><span class="glyphicon glyphicon-time"></span></span>
                        </g:else>
                    </td>
                    <td>
                        <g:if test="${tradeInstance.engagedReceived}">
                            <span class="flow-btn btn btn-success" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.received', args: [tradeInstance.engagedUser.name])}"><span class="glyphicon glyphicon-ok"></span></span>
                        </g:if>
                        <g:else>
                            <span class="flow-btn btn btn-warning" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.hasNotReceivedYet', args: [tradeInstance.engagedUser.name])}"><span class="glyphicon glyphicon-time"></span></span>
                        </g:else>
                    </td>
                    <td>
                        <g:if test="${tradeInstance.engagedReviewed}">
                            <g:if test="${tradeInstance.engagedPositiveReview}">
                                <span class="flow-btn btn btn-success" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.approved', args: [tradeInstance.engagedUser.name])}"><span class="glyphicon glyphicon-thumbs-up"></span></span>
                            </g:if>
                            <g:else>
                                <span class="flow-btn btn btn-danger" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.hasNotApproved', args: [tradeInstance.engagedUser.name])}"><span class="glyphicon glyphicon-thumbs-down"></span></span>
                            </g:else>
                        </g:if>
                        <g:else>
                            <span class="flow-btn btn btn-warning" data-toggle="tooltip" data-placement="left" data-original-title="${message(code: 'trade.flow.hasNotReviewedYet', args: [tradeInstance.engagedUser.name])}"><span class="glyphicon glyphicon-time"></span></span>
                        </g:else>
                    </td>
                </tr>
            </table>
        </div>
	</body>
</html>
