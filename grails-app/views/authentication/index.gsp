<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="lp"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'jquery.fancybox.css')}">
    <script type="text/javascript" src="${resource(dir:'js', file:'jquery.fancybox.pack.js')}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox();
            $('#big-button').click(function() {
                ga('send', 'event', 'button', 'click', 'big-button');
            });
        });
    </script>
</head>
<body>
    <a href="https://github.com/ziglee/urbanfarmtrading"><img style="position: absolute; top: 0; right: 0; border: 0;" src="https://s3.amazonaws.com/github/ribbons/forkme_right_gray_6d6d6d.png" alt="Fork me on GitHub"></a>

    <g:if test="${flash.message}">
        <div class="alert alert-info alert-dismissable" role="status">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            ${flash.message}
        </div>
    </g:if>

    <div class="jumbotron">
        <h1>Urban Farm Trading</h1>
        <p class="lead">Grow and trade food locally and get off-the-grid. A non-profit, self-sustainable community.</p>
        <p><g:link class="btn btn-lg btn-success" elementId="big-button" controller="authentication" action="create" role="button">Sign up today</g:link><g:link style="margin-left: 20px;" class="btn btn-lg btn-primary" elementId="big-button" controller="info" action="index" role="button">Check map</g:link></p>
    </div>

    <div class="row marketing">
        <div class="col-lg-6">
            <h4>Enviromental impact</h4>
            <p>Local trading requires less transportation. This generally means contributing less to sprawl, congestion, habitat loss and pollution.</p>

            <h4>By-pass taxes</h4>
            <p>It doesn't involves money, so you don't have to give satisfaction to the government about it.</p>

            <h4>By-pass regulations</h4>
            <p>A small business requires a lot of bureaucracy. Trading food is not a business so you don't have to worry about it.</p>
        </div>

        <div class="col-lg-6">
            <h4>It is sustainable</h4>
            <p>Large-scale, agribusiness-oriented food systems are bound to fail on the long term, sunk by their own unsustainability.</p>

            <h4>Economize</h4>
            <p>Without middlemen, the only cost you have is growing you own food.</p>

            <h4>Eat healthier</h4>
            <p>Smaller-scale organic agriculture produces better food, tastier, and more rich in nutrients. No GMO contaminations.</p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-4">
            <a class="fancybox" href="${resource(dir: 'images', file: 'screenshot_01.png')}" title="Dashboard">
                <div class="thumbnail">
                    <img src="${resource(dir: 'images', file: 'screenshot_01.png')}" data-src="holder.js/300x200" alt="...">
                    <div class="caption">
                        <h3>Dashboard</h3>
                        <p>See who is around you offering what you need.</p>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-sm-6 col-md-4">
            <a class="fancybox" href="${resource(dir: 'images', file: 'screenshot_02.png')}" title="Profile">
                <div class="thumbnail">
                    <img src="${resource(dir: 'images', file: 'screenshot_02.png')}" data-src="holder.js/300x200" alt="...">
                    <div class="caption">
                        <h3>Profile</h3>
                        <p>See what someone is offering, needing and his trades history with you.</p>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-sm-6 col-md-4">
            <a class="fancybox" href="${resource(dir: 'images', file: 'screenshot_03.png')}" title="Trade flow">
                <div class="thumbnail">
                    <img src="${resource(dir: 'images', file: 'screenshot_03.png')}" data-src="holder.js/300x200" alt="...">
                    <div class="caption">
                        <h3>Trade flow</h3>
                        <p>Take action to complete the trade and receving notifications by email.</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</body>
</html>
