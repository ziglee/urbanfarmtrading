<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="lp"/>
    <g:set var="entityName" value="${message(code: 'userProfile.label', default: 'UserProfile')}" />
    <title> - Contact</title>
    <script>
        $(document).ready(function() {
            $('#main-nav li').removeClass('active');
            $('#main-nav-contact').addClass('active');

            $('#create').click(function() {
                ga('send', 'event', 'contact', 'send');
            });
        });
    </script>
</head>
<body>
<g:if test="${flash.message}">
    <div class="alert alert-danger alert-dismissable" role="status">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        ${flash.message}
    </div>
</g:if>

<div class="jumbotron">
<g:form action="sendContact" class="form-horizontal" role="form">
    <fieldset class="form">
        <div class="form-group fieldcontain">
            <label for="name" class="col-sm-2 control-label">
                <g:message code="name.label" default="Name" />
            </label>
            <div class="col-sm-8">
                <g:textField name="name" class="form-control" value="${params.name}" maxlength="250"/>
            </div>
        </div>

        <div class="form-group fieldcontain">
            <label for="email" class="col-sm-2 control-label">
                <g:message code="email.label" />
            </label>
            <div class="col-sm-8">
                <g:textField name="email" class="form-control" value="${params.email}" maxlength="250"/>
            </div>
        </div>

        <div class="form-group fieldcontain">
            <label for="message" class="col-sm-2 control-label">
                <g:message code="message.label" />
            </label>
            <div class="col-sm-8">
                <g:textArea name="message" class="form-control" value="${params.message}" maxlength="500"/>
            </div>
        </div>

        <div class="form-group fieldcontain required">
            <label for="recaptcha" class="col-sm-2 control-label">
            </label>
            <div class="col-sm-8">
                <recaptcha:recaptcha />
            </div>
        </div>
    </fieldset>
    <fieldset class="buttons">
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.send.label', default: 'Send')}" />
            </div>
        </div>
    </fieldset>
</g:form>
</div>
</body>
</html>