<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="lp"/>
    <style>
    .form-signin {
        max-width: 390px;
        padding: 15px;
        margin: 0 auto;
    }
    .form-signin .form-signin-heading,
    .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin .checkbox {
        font-weight: normal;
    }
    .form-signin .form-control {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 10px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    .form-signin .form-control:focus {
        z-index: 2;
    }
    .form-signin input[type="text"] {
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
        margin-bottom: 10px;
    }
    </style>
</head>
<body>
    <g:if test="${flash.message}">
        <div class="alert alert-info alert-dismissable" role="status">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            ${flash.message}
        </div>
    </g:if>

    <div class="jumbotron">
        <g:form controller="authentication" action="sendChangePasswordLink" class="form-signin">
            <input name="email" type="text" class="form-control" placeholder="${message(code: 'login.email')}" required autofocus>
            <button class="btn btn-lg btn-success btn-block" type="submit"><g:message code="requestNewPassword" /></button>
        </g:form>
    </div>
</body>
</html>
