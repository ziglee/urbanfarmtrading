<%@ page import="urbanfarmtrading.UserProfile" %>
<!DOCTYPE html>
<html>
	<head>
        <meta name="layout" content="lp"/>
		<g:set var="entityName" value="${message(code: 'userProfile.label', default: 'UserProfile')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoYE4hTspwbzPaQnkfix4OOo_ug-gDkzQ&sensor=false">
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#create').click(function() {
                    ga('send', 'event', 'user', 'create');
                });
            });

            function initializeMap() {
                var mapOptions = {
                    streetViewControl: false,
                    zoom: 4
                };

                var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

                if(navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var pos = new google.maps.LatLng(position.coords.latitude,
                                position.coords.longitude);

                        $('#latitude').val(position.coords.latitude);
                        $('#longitude').val(position.coords.longitude);

                        map.setCenter(pos);

                        var marker = new google.maps.Marker({
                            draggable: true,
                            position: pos,
                            map: map
                        });

                        google.maps.event.addListener(marker, "dragend", function(event) {
                            var point = marker.getPosition();
                            map.panTo(point);
                            $('#latitude').val(point.lat());
                            $('#longitude').val(point.lng());
                        });
                    }, function() {
                        handleNoGeolocation(true);
                    });
                } else {
                    handleNoGeolocation(false);
                }
            }

            google.maps.event.addDomListener(window, 'load', initializeMap);

            function handleNoGeolocation(opt) {
                alert('Geolocation not supported');
            }
        </script>
	</head>
	<body>
        <g:if test="${flash.message}">
            <div class="alert alert-info alert-dismissable" role="status">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                ${flash.message}
            </div>
        </g:if>

        <g:hasErrors bean="${userProfileInstance}">
            <div class="alert alert-danger alert-dismissable" role="status">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                <ul class="errors" role="alert">
                    <g:eachError bean="${userProfileInstance}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                    </g:eachError>
                </ul>
            </div>
        </g:hasErrors>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><g:message code="userProfile.dragCursorMap" /></h3>
            </div>
            <div class="panel-body">
                <div id="map-canvas" style="width: 100%; height: 300px"></div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><g:message code="default.create.label" args="[entityName]" /></h3>
            </div>
            <div class="panel-body">
                <g:form action="save" class="form-horizontal" role="form">
                    <g:hiddenField name="latitude" value="${userProfileInstance.latitude}"/>
                    <g:hiddenField name="longitude" value="${userProfileInstance.longitude}"/>

                    <fieldset class="form">
                        <div class="form-group fieldcontain ${hasErrors(bean: userProfileInstance, field: 'name', 'error')} required">
                            <label for="name" class="col-sm-2 control-label">
                                <g:message code="userProfile.name.label" default="Name" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="col-sm-8">
                                <g:textField name="name" class="form-control" value="${userProfileInstance?.name}" maxlength="250"/>
                            </div>
                        </div>

                        <div class="form-group fieldcontain ${hasErrors(bean: userProfileInstance, field: 'email', 'error')} required">
                            <label for="email" class="col-sm-2 control-label">
                                <g:message code="userProfile.email.label" default="E-mail" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="col-sm-8">
                                <g:textField name="email" class="form-control" value="${userProfileInstance?.email}" maxlength="250"/>
                            </div>
                        </div>

                        <div class="form-group fieldcontain ${hasErrors(bean: userProfileInstance, field: 'address', 'error')} required">
                            <label for="address" class="col-sm-2 control-label">
                                <g:message code="userProfile.address.label" default="Address" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="col-sm-8">
                                <g:textField name="address" class="form-control" value="${userProfileInstance?.address}" maxlength="250"/>
                            </div>
                        </div>

                        <div class="form-group fieldcontain ${hasErrors(bean: userProfileInstance, field: 'password', 'error')} required">
                            <label for="password" class="col-sm-2 control-label">
                                <g:message code="userProfile.password.label" default="Password" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="col-sm-8">
                                <g:passwordField name="password" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group fieldcontain required">
                            <label for="password" class="col-sm-2 control-label">
                                <g:message code="userProfile.repassword.label" />
                                <span class="required-indicator">*</span>
                            </label>
                            <div class="col-sm-8">
                                <g:passwordField name="repassword" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group fieldcontain ${hasErrors(bean: userProfileInstance, field: 'password', 'error')} required">
                            <label for="recaptcha" class="col-sm-2 control-label">
                            </label>
                            <div class="col-sm-8">
                                <recaptcha:recaptcha />
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="buttons">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-8">
                                <g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                            </div>
                        </div>
                    </fieldset>
                </g:form>
            </div>
        </div>
	</body>
</html>
