<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="lp"/>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#main-nav li').removeClass('active');
            $('#main-nav-login').addClass('active');

            $('#sign-in').click(function() {
                ga('send', 'event', 'button', 'click', 'sign-in');
            });

            $('#sign-up').click(function() {
                ga('send', 'event', 'button', 'click', 'sign-up');
            });
        });
    </script>
</head>
<body>
    <g:if test="${flash.message}">
        <div class="alert alert-info alert-dismissable" role="status">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            ${flash.message}
        </div>
    </g:if>

    <div class="jumbotron">
        <g:form controller="authentication" action="authenticate" class="form-signin">
            <input name="email" type="text" class="form-control" placeholder="${message(code: 'login.email')}" required>
            <input name="password" type="password" class="form-control" placeholder="${message(code: 'login.password')}" required>
            <button class="btn btn-lg btn-success btn-block" type="submit" id="sign-in"><g:message code="signIn" /></button>
            <g:link controller="authentication" action="create" class="btn btn-lg btn-primary btn-block" elementId="sign-up"><g:message code="signUp" /></g:link>
            <g:link controller="authentication" action="askForChangePasswordLink"><g:message code="forgotMyPassword" default="Esqueci minha senha" /></g:link>
        </g:form>
    </div>
</body>
</html>
