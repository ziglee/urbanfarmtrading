<%@ page import="urbanfarmtrading.AdminUser" %>

<div class="fieldcontain ${hasErrors(bean: adminUserInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="adminUser.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${adminUserInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: adminUserInstance, field: 'login', 'error')} required">
	<label for="login">
		<g:message code="adminUser.login.label" default="Login" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="login" required="" value="${adminUserInstance?.login}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: adminUserInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="adminUser.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="password" name="password" />
</div>
