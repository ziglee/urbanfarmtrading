<%@ page import="urbanfarmtrading.UserProfile" %>

<g:hiddenField name="latitude" value="${userProfileInstance.latitude}"/>
<g:hiddenField name="longitude" value="${userProfileInstance.longitude}"/>

<div class="form-group fieldcontain ${hasErrors(bean: userProfileInstance, field: 'name', 'error')} required">
    <label for="name" class="col-sm-2 control-label">
        <g:message code="userProfile.name.label" default="Name" />
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-10">
        <g:textField name="name" value="${userProfileInstance.name}" class="form-control" maxlength="250"/>
    </div>
</div>

<div class="form-group fieldcontain ${hasErrors(bean: userProfileInstance, field: 'address', 'error')} required">
    <label for="address" class="col-sm-2 control-label">
        <g:message code="userProfile.address.label" default="Address" />
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-10">
        <g:textField name="address" value="${userProfileInstance.address}" class="form-control" maxlength="500"/>
    </div>
</div>