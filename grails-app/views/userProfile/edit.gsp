<%@ page import="urbanfarmtrading.UserProfile" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'userProfile.label', default: 'UserProfile')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoYE4hTspwbzPaQnkfix4OOo_ug-gDkzQ&sensor=false">
        </script>
        <script>
            $(document).ready(function() {
                $('#main-nav li').removeClass('active');
                <g:if test="${session.userProfile.id == userProfileInstance.id}">
                $('#right-nav-profile').addClass('active');
                </g:if>
            });

            function initializeMap() {
                var pos = new google.maps.LatLng(${latLongFormat.format(userProfileInstance.latitude)},
                        ${latLongFormat.format(userProfileInstance.longitude)});

                var mapOptions = {
                    streetViewControl: false,
                    center: pos,
                    zoom: 14
                };

                var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
                var marker = new google.maps.Marker({
                    draggable: true,
                    position: pos,
                    map: map
                });

                google.maps.event.addListener(marker, "dragend", function(event) {
                    var point = marker.getPosition();
                    map.panTo(point);
                    $('#latitude').val(point.lat());
                    $('#longitude').val(point.lng());
                });
            }

            google.maps.event.addDomListener(window, 'load', initializeMap);
        </script>
	</head>
	<body>
        <g:if test="${flash.message}">
            <div class="alert alert-info alert-dismissable" role="status">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                ${flash.message}
            </div>
        </g:if>

        <g:hasErrors bean="${userProfileInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${userProfileInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
        </g:hasErrors>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><g:message code="default.edit.label" args="[entityName]" /></h3>
            </div>
            <div class="panel-body">
                <g:form method="post" class="form-horizontal" role="form">
                    <g:hiddenField name="id" value="${userProfileInstance?.id}" />
                    <g:hiddenField name="version" value="${userProfileInstance?.version}" />
                    <fieldset class="form">
                        <g:render template="form"/>
                    </fieldset>
                    <fieldset class="buttons">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                                <g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                            </div>
                        </div>
                    </fieldset>
                </g:form>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><g:message code="userProfile.dragCursorMap" /></h3>
            </div>
            <div class="panel-body">
                <div id="map-canvas" style="width: 100%; height: 500px"></div>
            </div>
        </div>
	</body>
</html>
