<%@ page import="urbanfarmtrading.UserProfile" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'userProfile.label', default: 'UserProfile')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
        <script>
            $(document).ready(function() {
                $('#main-nav li').removeClass('active');
                <g:if test="${session.userProfile.id == userProfileInstance.id}">
                    $('#right-nav-profile').addClass('active');
                </g:if>
                $('.food-offer-tooltip').tooltip();
                $('.food-demand-popover').popover();
            });
        </script>
	</head>
	<body>
        <g:if test="${flash.message}">
            <div class="alert alert-info alert-dismissable" role="status">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                ${flash.message}
            </div>
        </g:if>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-info-sign"></span> ${userProfileInstance.name.encodeAsHTML()} (<g:link controller="userProfileDashboard" params="[latitude: userProfileInstance.latitude, longitude: userProfileInstance.longitude]"><g:message code="showOnMap" /></g:link>)</h3>
            </div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt></dt>
                    <dd><img src="${userProfileInstance.gravatarUrl}" /></dd>

                    <g:if test="${userProfileInstance.ratingCount}">
                        <dt><g:message code="userProfile.rating.label" default="Rating" /></dt>
                        <dd>${userProfileInstance.rating}</dd>
                    </g:if>

                    <g:if test="${userProfileInstance.id == session.userProfile.id}">
                        <dt><g:message code="userProfile.email.label" default="E-mail" /></dt>
                        <dd>${userProfileInstance.email.encodeAsHTML()}</dd>
                        <dt><g:message code="userProfile.address.label" default="Address" /></dt>
                        <dd>${userProfileInstance.address.encodeAsHTML()}</dd>
                        <dt></dt>
                        <dd>
                            <g:form>
                                <fieldset class="buttons">
                                    <g:hiddenField name="id" value="${userProfileInstance?.id}" />
                                    <g:link class="btn btn-primary" action="edit" id="${userProfileInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                                </fieldset>
                            </g:form>
                        </dd>
                    </g:if>
                </dl>
            </div>
        </div>

        <g:if test="${session.userProfile.id != userProfileInstance.id}">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="glyphicon glyphicon-cutlery"></span> <g:message code="foodOffer.plural.label" /></h3>
                        </div>
                        <table class="table table-hover">
                            <g:each in="${foodOffers}" var="foodOffer">
                                <tr>
                                    <td><span class="food-offer-tooltip" data-toggle="tooltip" data-placement="right" title="${foodOffer.comment.encodeAsHTML()}">${foodOffer.food.name.encodeAsHTML()}</span></td>
                                    <td style="text-align: right;"><span class="badge"><g:formatNumber number="${foodOffer.kilograms}" minFractionDigits="1"/> kg</span></td>
                                </tr>
                            </g:each>
                            <tr class="active">
                                <td colspan="2">
                                    <g:form>
                                        <fieldset class="buttons">
                                            <g:hiddenField name="id" value="${userProfileInstance?.id}" />
                                            <g:link id="start" class="btn btn-primary" controller="trade" action="create" params="['engagedUser.id': userProfileInstance?.id]"><g:message code="trade.startTrading" /></g:link>
                                        </fieldset>
                                    </g:form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="glyphicon glyphicon-cutlery"></span> <g:message code="foodDemand.plural.label" /></h3>
                        </div>
                        <table class="table table-hover">
                            <g:each in="${foodDemands}" var="foodDemand">
                                <tr>
                                    <td><span class="food-offer-popover" data-toggle="popover">${foodDemand.food.name.encodeAsHTML()}</span></td>
                                </tr>
                            </g:each>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="glyphicon glyphicon-transfer"></span> <g:message code="trade.withMe.plural.label" /></h3>
                </div>
                <table class="table table-hover">
                    <tr>
                        <th><g:message code="trade.status.label" /></th>
                        <th><g:message code="trade.lastUpdated.label" /></th>
                        <th><g:message code="trade.dateCreated.label" /></th>
                    </tr>
                    <g:each in="${trades}" var="trade">
                        <tr>
                            <td><g:link controller="trade" action="show" id="${trade.id}"><g:message code="${trade.status.code}"/></g:link></td>
                            <td><g:formatDate date="${trade.lastUpdated}" type="datetime" style="SHORT"/></td>
                            <td><g:formatDate date="${trade.dateCreated}" type="datetime" style="SHORT"/></td>
                        </tr>
                    </g:each>
                </table>
            </div>
        </g:if>
	</body>
</html>
