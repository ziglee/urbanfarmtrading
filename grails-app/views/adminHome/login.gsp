<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="lp"/>
    <title>Urban Farm Trading</title>
    <style>
    body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #eee;
    }

    .form-signin {
        max-width: 390px;
        padding: 15px;
        margin: 0 auto;
    }
    .form-signin .form-signin-heading,
    .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin .checkbox {
        font-weight: normal;
    }
    .form-signin .form-control {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 10px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    .form-signin .form-control:focus {
        z-index: 2;
    }
    .form-signin input[type="text"] {
        margin-bottom: -1px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }
    .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
    </style>
</head>
<body>
<div class="container">
    <g:form controller="adminHome" action="authenticate" class="form-signin">
        <h2 class="form-signin-heading"><img src="${resource(dir: 'images', file: 'logo.png')}" width="60px"/> Urban Farm Trading</h2>

        <g:if test="${flash.message}">
            <div class="alert alert-info alert-dismissable" role="status">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                ${flash.message}
            </div>
        </g:if>

        <input name="login" type="text" class="form-control" placeholder="Login" required autofocus>
        <input name="password" type="password" class="form-control" placeholder="${message(code: 'login.password')}" required>
        <button class="btn btn-lg btn-success btn-block" type="submit"><g:message code="signIn" /></button>
    </g:form>
</div>
</body>
</html>
