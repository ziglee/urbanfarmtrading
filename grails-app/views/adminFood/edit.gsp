<%@ page import="urbanfarmtrading.Food" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'food.label', default: 'Food')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
        <script>
            $(document).ready(function() {
                $('#main-nav li').removeClass('active');
                $('#main-nav-food').addClass('active');
            });
        </script>
	</head>
	<body>
        <g:if test="${flash.message}">
            <div class="alert alert-info alert-dismissable" role="status">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                ${flash.message}
            </div>
        </g:if>

        <g:hasErrors bean="${foodInstance}">
            <ul class="errors" role="alert">
                <g:eachError bean="${foodInstance}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
        </g:hasErrors>

		<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><g:message code="default.edit.label" args="[entityName]" /></h3>
            </div>
            <div class="panel-body">
                <g:form method="post" class="form-horizontal" role="form">
                    <g:hiddenField name="id" value="${foodInstance?.id}" />
                    <g:hiddenField name="version" value="${foodInstance?.version}" />
                    <fieldset class="form">
                        <g:render template="form"/>
                    </fieldset>
                    <fieldset class="buttons">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                                <g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                            </div>
                        </div>
                    </fieldset>
                </g:form>
            </div>
        </div>
	</body>
</html>
