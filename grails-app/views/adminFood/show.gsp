<%@ page import="urbanfarmtrading.Food" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'food.label', default: 'Food')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
        <script>
            $(document).ready(function() {
                $('#main-nav li').removeClass('active');
                $('#main-nav-food').addClass('active');
            });
        </script>
	</head>
	<body>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>

		<div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-info-sign"></span> <g:message code="default.show.label" args="[entityName]" /></h3>
            </div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <g:if test="${foodInstance.photoFilename}">
                        <dt></dt>
                        <dd><img src="${foodInstance.imgSrcThumbCropped}"/></dd>
                    </g:if>
                    <dt><g:message code="food.type.label" /></dt>
                    <dd><g:message code="${foodInstance.type.code}"/></dd>
                    <dt><g:message code="food.name.label" /></dt>
                    <dd>${foodInstance.name.encodeAsHTML()}</dd>
                </dl>
            </div>
        </div>

        <g:form>
            <fieldset class="buttons">
                <g:hiddenField name="id" value="${foodInstance?.id}" />
                <g:link class="edit" action="edit" id="${foodInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
            </fieldset>
        </g:form>
	</body>
</html>
