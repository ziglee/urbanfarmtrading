<%@ page import="urbanfarmtrading.Food" %>

<div class="form-group fieldcontain ${hasErrors(bean: foodInstance, field: 'name', 'error')} required">
    <label for="name" class="col-sm-2 control-label">
		<g:message code="food.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
    <div class="col-sm-10">
	    <g:textField name="name" maxlength="150" required="" value="${foodInstance?.name}" class="form-control"/>
    </div>
</div>

<div class="form-group fieldcontain ${hasErrors(bean: foodInstance, field: 'type', 'error')} required">
    <label for="type" class="col-sm-2 control-label">
        <g:message code="food.type.label" />
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-10">
        <g:select name="type" from="${urbanfarmtrading.FoodType?.values()}" keys="${urbanfarmtrading.FoodType.values()*.name()}" value="${foodInstance?.type?.name()}" optionValue="code" class="form-control"/>
    </div>
</div>