<%@ page import="urbanfarmtrading.Food" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="admin">
		<g:set var="entityName" value="${message(code: 'food.label', default: 'Food')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
        <script>
            $(document).ready(function() {
                $('#main-nav li').removeClass('active');
                $('#main-nav-food').addClass('active');
            });
        </script>
	</head>
	<body>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>

		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>

        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <g:sortableColumn property="name" title="${message(code: 'food.name.label', default: 'Name')}" />
                    <g:sortableColumn property="type" title="${message(code: 'food.type.label', default: 'Type')}" />
                </tr>
            </thead>
            <tbody>
            <g:each in="${foodInstanceList}" status="i" var="foodInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                    <td><g:link action="show" id="${foodInstance.id}">${fieldValue(bean: foodInstance, field: "name")}</g:link></td>
                    <td><g:message code="${foodInstance.type.code}" /></td>
                </tr>
            </g:each>
            </tbody>
        </table>

        <div class="pagination">
            <g:paginate total="${foodInstanceTotal}" />
        </div>
	</body>
</html>
