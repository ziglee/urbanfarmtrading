<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="unlogged" />
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoYE4hTspwbzPaQnkfix4OOo_ug-gDkzQ&sensor=false">
        </script>
        <script type="text/javascript">
            var markers = [];
            var map = null;
            var markersUrl = '${createLink(action: 'listUserProfilesByLatLngBounds')}';

            function mapBoundsChanged() {
                $.ajax({
                    url: markersUrl,
                    contentType: 'application/json',
                    dataType: 'json',
                    data: {
                        northEastLatitude: map.getBounds().getNorthEast().lat(),
                        northEastLongitude: map.getBounds().getNorthEast().lng(),
                        southWestLatitude: map.getBounds().getSouthWest().lat(),
                        southWestLongitude: map.getBounds().getSouthWest().lng()
                    },
                    success: function(doc) {
                        if (doc.error == 0) {
                            clearMarkers();
                            $.each(doc.payload, function(k, v) {
                                addMarker(map, v.latitude, v.longitude, v.name, v.url);
                            });
                        } else {
                            alert(doc.payload);
                        }
                    },
                    error: function() {
                        alert('<g:message code="error" />');
                    }
                });
            }

            function initializeMap() {
                var mapOptions = {
                    <g:if test="${params.latitude && params.longitude}">
                        center: new google.maps.LatLng(${latLongFormat.format(params.double('latitude'))}, ${latLongFormat.format(params.double('longitude'))}),
                    </g:if>
                    <g:else>
                        center: new google.maps.LatLng(40, -90),
                    </g:else>
                    zoom: 5
                };

                map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

                google.maps.event.addListener(map, 'bounds_changed', mapBoundsChanged);
            }

            google.maps.event.addDomListener(window, 'load', initializeMap);

            function addMarker(map, latitude, longitude, title, url) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(latitude, longitude),
                    icon: '${resource(dir:'images', file:'map_pin_alt.png')}',
                    title: title,
                    map: map
                });

                markers.push(marker);
            }

            function setAllMap(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            function clearMarkers() {
                setAllMap(null);
                markers = [];
            }
        </script>
    </head>
    <body>
        <div class="panel panel-default">
            <div class="panel-body">
                <div id="map-canvas" style="width: 100%; height: 500px"></div>
            </div>
        </div>
    </body>
</html>