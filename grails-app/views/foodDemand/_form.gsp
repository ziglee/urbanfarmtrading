<%@ page import="urbanfarmtrading.FoodDemand" %>

<script>
    $(document).ready(function() {
        $('#foodType').change(foodTypeChanged);

        <g:if test="${foodDemandInstance?.food}">
        $('#foodType').val('${foodDemandInstance.food.type.name()}');
        </g:if>

        foodTypeChanged();
    });

    function foodTypeChanged() {
        var value = $('#foodType').val();
        $('#food').empty();

        $.ajax({
            url: '${createLink(controller: 'food', action: 'listFoodByTypeJson')}',
            dataType: 'json',
            type: 'POST',
            data: {
                'type': value
            },
            success: function(doc) {
                if (doc.error == 0) {
                    $.each(doc.payload, function(keyp, valp) {
                        $('#food').append("<option data-value='"+ valp.value +"' value='"+ valp.id +"'>"+ valp.name + "</option>");
                    });

                    <g:if test="${foodDemandInstance?.food}">
                        $('#food').val(${foodDemandInstance.food.id.encodeAsJavaScript()});
                    </g:if>
                } else {
                    alert(doc.payload);
                }
            },
            error: function() {
                alert('<g:message code="error" />');
            }
        });
    }
</script>

<div class="form-group fieldcontain required">
    <label for="foodType" class="col-sm-2 control-label">
        <g:message code="food.type.label" />
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-10">
        <select id="foodType" class="form-control">
            <g:each in="${urbanfarmtrading.FoodType?.values()}" var="type">
                <option value="${type.name()}"><g:message code="${type.code}" /></option>
            </g:each>
        </select>
    </div>
</div>

<div class="form-group fieldcontain ${hasErrors(bean: foodDemandInstance, field: 'food', 'error')} required">
    <label for="food" class="col-sm-2 control-label">
        <g:message code="food.label" default="Food" />
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-10">
        <select id="food" name="food.id" class="form-control"></select>
    </div>
</div>