<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Urban Farm Trading<g:layoutTitle/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Grow and trade food locally and get off-the-grid. A non-profit, self-sustainable community.">
        <meta name="keywords" content="urban, farm, barter, trading, organic, food">
        <meta name="author" content="Cassio Landim">
        <link rel="shortcut icon" href="${resource(dir: 'images', file: 'organic.png')}" type="image/png">

        <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}">
        <script type="text/javascript" src="${resource(dir:'js', file:'jquery-1.9.1.js')}"></script>
        <script type="text/javascript" src="${resource(dir:'js', file: 'jquery.numeric.js')}"></script>
        <script src="${resource(dir: 'js', file: 'bootstrap.min.js')}"></script>
        <style>
        /* Space out content a bit */
        body {
            padding-top: 20px;
            padding-bottom: 20px;
        }

        /* Everything but the jumbotron gets side spacing for mobile first views */
        .header,
        .marketing,
        .footer {
            padding-left: 15px;
            padding-right: 15px;
        }

        /* Custom page header */
        .header {
            border-bottom: 1px solid #e5e5e5;
        }
        /* Make the masthead heading the same height as the navigation */
        .header h3 {
            margin-top: 0;
            margin-bottom: 0;
            line-height: 40px;
            padding-bottom: 19px;
        }

        /* Custom page footer */
        .footer {
            padding-top: 19px;
            color: #777;
            border-top: 1px solid #e5e5e5;
        }

        /* Customize container */
        @media (min-width: 768px) {
            .container {
                max-width: 730px;
            }
        }
        .container-narrow > hr {
            margin: 30px 0;
        }

        /* Main marketing message and sign up button */
        .jumbotron {
            text-align: center;
            border-bottom: 1px solid #e5e5e5;
        }
        .jumbotron .btn {
            font-size: 21px;
            padding: 14px 24px;
        }

        /* Supporting marketing content */
        .marketing {
            margin: 40px 0;
        }
        .marketing p + h4 {
            margin-top: 28px;
        }

        /* Responsive: Portrait tablets and up */
        @media screen and (min-width: 768px) {
            /* Remove the padding we set earlier */
            .header,
            .marketing,
            .footer {
                padding-left: 0;
                padding-right: 0;
            }
            /* Space out the masthead */
            .header {
                margin-bottom: 30px;
            }
            /* Remove the bottom border on the jumbotron for visual effect */
            .jumbotron {
                border-bottom: 0;
            }
        }

        .form-signin {
            max-width: 390px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }
        .form-signin .checkbox {
            font-weight: normal;
        }
        .form-signin .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="text"] {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        </style>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.stats.g.doubleclick.net/dc.js','ga');

            ga('create', 'UA-46453398-1', 'urbanfarmtrading.org');
            ga('send', 'pageview');
        </script>
        <g:layoutHead/>
		<r:layoutResources/>
	</head>
	<body>
        <div class="container">
            <div class="header">
                <ul class="nav nav-pills pull-right" id="main-nav">
                    <li id="main-nav-home" class="active"><g:link uri="/"><g:message code="home" /></g:link></li>
                    <li id="main-nav-login"><g:link controller="authentication" action="login">Login</g:link></li>
                    <li id="main-nav-contact"><g:link controller="authentication" action="contact">Contact</g:link></li>
                    <li id="main-nav-plus"><a href="https://plus.google.com/107263919515868539103" rel="publisher">Google+</a></li>
                </ul>
                <h3 class="text-muted">Urban Farm Trading</h3>
            </div>
            <g:layoutBody/>
            <div class="footer">
                <p style="font-size: xx-small"><a href="http://opensource.org"><img src="${resource(dir: 'images', file: 'osi_standard_logo.png')}" width="50px"/></a> "The OSI logo trademark is the trademark of Open Source Initiative" "This site is not affiliated with or endorsed by the Open Source Initiative"</p>
                <p style="font-size: xx-small">“Organic” symbol by Jens Tärning, from <a href="http://thenounproject.com/jens/">The Noun Project</a> collection</p>
                <p style="font-size: x-small"><a href="http://www.linkedin.com/in/cassiolandim">By Cassio Landim</a></p>
            </div>
        </div>

        <g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>
