<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">

		<title>Urban Farm Trading<g:layoutTitle/></title>

        <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}">
        <script type="text/javascript" src="${resource(dir:'js', file:'jquery-1.9.1.js')}"></script>
        <script type="text/javascript" src="${resource(dir:'js', file: 'jquery.numeric.js')}"></script>
        <script src="${resource(dir: 'js', file: 'bootstrap.min.js')}"></script>

        <style>
            body {
                padding-top: 70px;
            }
        </style>

        <g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <g:link class="navbar-brand" controller="adminDashboard">UrbanFarmTrading</g:link>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav" id="main-nav">
                        <li id="main-nav-home" class="active"><g:link controller="adminDashboard"><g:message code="home" /></g:link></li>
                        <li id="main-nav-food"><g:link controller="adminFood"><g:message code="food.plural.label" /></g:link></li>
                        <li id="main-nav-food"><g:link controller="adminUser"><g:message code="adminUser.plural.label" /></g:link></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li id="right-nav-profile" ><a href="#">${session.adminUser.name}</a></li>
                        <li><g:link controller="authentication" action="logout"><g:message code="logout"/></g:link></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>

        <div class="container">
            <g:layoutBody/>
            <div class="footer" role="contentinfo"></div>
            <div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
        </div>
		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>
