<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Grow and trade food locally and et off-the-grid.">
        <meta name="keywords" content="urban, farm, barter, trading, organic, food">
        <meta name="author" content="Cassio Landim">
        <link rel="shortcut icon" href="${resource(dir: 'images', file: 'organic.png')}" type="image/png">

		<title>Urban Farm Trading<g:layoutTitle/></title>

        <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}">
        <script type="text/javascript" src="${resource(dir:'js', file:'jquery-1.9.1.js')}"></script>
        <script type="text/javascript" src="${resource(dir:'js', file: 'jquery.numeric.js')}"></script>
        <script src="${resource(dir: 'js', file: 'bootstrap.min.js')}"></script>

        <style>
            body {
                padding-top: 70px;
            }
        </style>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-46453398-1', 'urbanfarmtrading.org');
            ga('send', 'pageview');
        </script>

        <g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <g:link class="navbar-brand" controller="userProfileDashboard">Urban Farm Trading</g:link>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li id="right-nav-profile"><g:link controller="authentication" action="login">Login</g:link></li>
                    <li><g:link controller="authentication" action="contact">Contact</g:link></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>

    <div class="container">
        <g:layoutBody/>
        <div class="footer">
            <p style="font-size: xx-small"><a href="http://opensource.org"><img src="${resource(dir: 'images', file: 'osi_standard_logo.png')}" width="50px"/></a> "The OSI logo trademark is the trademark of Open Source Initiative" "This site is not affiliated with or endorsed by the Open Source Initiative"</p>
            <p style="font-size: xx-small">“Organic” symbol by Jens Tärning, from <a href="http://thenounproject.com/jens/">The Noun Project</a> collection</p>
            <p style="font-size: x-small"><a href="http://www.linkedin.com/in/cassiolandim">By Cassio Landim</a></p>
        </div>
    </div>
    <g:javascript library="application"/>
    <r:layoutResources />
	</body>
</html>
