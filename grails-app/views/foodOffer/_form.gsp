<%@ page import="urbanfarmtrading.FoodOffer" %>

<script>
    $(document).ready(function() {
        $('#foodType').change(foodTypeChanged);

        <g:if test="${foodOfferInstance?.food}">
        $('#foodType').val('${foodOfferInstance.food.type.name()}');
        </g:if>

        foodTypeChanged();
    });

    function foodTypeChanged() {
        var value = $('#foodType').val();
        $('#food').empty();

        $.ajax({
            url: '${createLink(controller: 'food', action: 'listFoodByTypeJson')}',
            dataType: 'json',
            type: 'POST',
            data: {
                'type': value
            },
            success: function(doc) {
                if (doc.error == 0) {
                    $.each(doc.payload, function(keyp, valp) {
                        $('#food').append("<option data-value='"+ valp.value +"' value='"+ valp.id +"'>"+ valp.name + "</option>");
                    });

                <g:if test="${foodOfferInstance?.food}">
                    $('#food').val(${foodOfferInstance.food.id.encodeAsJavaScript()});
                </g:if>
                } else {
                    alert(doc.payload);
                }
            },
            error: function() {
                alert('<g:message code="error" />');
            }
        });
    }
</script>

<div class="form-group fieldcontain required">
    <label for="foodType" class="col-sm-2 control-label">
        <g:message code="food.type.label" />
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-10">
        <select id="foodType" class="form-control">
            <g:each in="${urbanfarmtrading.FoodType?.values()}" var="type">
                <option value="${type.name()}"><g:message code="${type.code}" /></option>
            </g:each>
        </select>
    </div>
</div>

<div class="form-group fieldcontain ${hasErrors(bean: foodOfferInstance, field: 'food', 'error')} required">
    <label for="food" class="col-sm-2 control-label">
        <g:message code="food.label" default="Food" />
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-10">
        <select id="food" name="food.id" class="form-control"></select>
    </div>
</div>

<div class="form-group fieldcontain ${hasErrors(bean: foodOfferInstance, field: 'kilograms', 'error')} required">
    <label for="kilograms" class="col-sm-2 control-label">
        <g:message code="foodOffer.kilograms.label" default="Kilograms" />
        <span class="required-indicator">*</span>
    </label>
    <div class="col-sm-10">
        <g:textField name="kilograms" value="${formatNumber(number: foodOfferInstance.kilograms, minFractionDigits: 2)}" required="" class="form-control"/>
    </div>
</div>

<div class="form-group fieldcontain ${hasErrors(bean: foodOfferInstance, field: 'availableUntil', 'error')} required">
	<label for="availableUntil" class="col-sm-2 control-label">
		<g:message code="foodOffer.availableUntil.label" default="Available Until" />
		<span class="required-indicator">*</span>
	</label>
    <div class="col-sm-10">
	    <g:datePicker name="availableUntil" precision="day"  value="${foodOfferInstance?.availableUntil}" class="form-control"/>
    </div>
</div>

<div class="form-group fieldcontain ${hasErrors(bean: foodOfferInstance, field: 'comment', 'error')} ">
    <label for="comment" class="col-sm-2 control-label">
        <g:message code="foodOffer.comment.label" default="Comment" />
    </label>
    <div class="col-sm-10">
        <g:textArea name="comment" cols="40" rows="5" maxlength="400" value="${foodOfferInstance?.comment}" class="form-control"/>
    </div>
</div>