package urbanfarmtrading

import org.joda.time.DateTime

class UserProfileHasOffersJob {

    static triggers = {
        cron name: 'UserProfileHasOffersJobTrigger', cronExpression: "0 0 6 * * ?"
    }

    def execute() {
        FoodOffer.executeQuery('select f from FoodOffer f where f.availableUntil < :now', [now: new DateTime().withTimeAtStartOfDay().toDate()]).each {
            it.delete(flush: true)
        }
        UserProfile.findAllByHasOffers(true).each {
            it.updateHasOffers()
        }
    }
}
