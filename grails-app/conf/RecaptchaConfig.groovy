recaptcha {
    publicKey = ""
    privateKey = ""
    includeNoScript = true
    forceLanguageInURL = false
    enabled = true
    useSecureAPI = true
}

mailhide {
    publicKey = ""
    privateKey = ""
}