CREATE TABLE `admin_user` (`id` BIGINT AUTO_INCREMENT NOT NULL, `version` BIGINT NOT NULL, `login` VARCHAR(255) NOT NULL, `name` VARCHAR(255) NOT NULL, `password` VARCHAR(255) NOT NULL, CONSTRAINT `admin_userPK` PRIMARY KEY (`id`)) ENGINE=InnoDB;
CREATE TABLE `food` (`id` BIGINT AUTO_INCREMENT NOT NULL, `version` BIGINT NOT NULL, `name` VARCHAR(150) NOT NULL, `photo_filename` VARCHAR(255) NULL, `type` BIGINT NOT NULL, CONSTRAINT `foodPK` PRIMARY KEY (`id`)) ENGINE=InnoDB;
CREATE TABLE `food_demand` (`id` BIGINT AUTO_INCREMENT NOT NULL, `version` BIGINT NOT NULL, `food_id` BIGINT NOT NULL, `user_profile_id` BIGINT NOT NULL, CONSTRAINT `food_demandPK` PRIMARY KEY (`id`)) ENGINE=InnoDB;
CREATE TABLE `food_offer` (`id` BIGINT AUTO_INCREMENT NOT NULL, `version` BIGINT NOT NULL, `available_until` DATETIME NOT NULL, `comment` VARCHAR(400) NULL, `food_id` BIGINT NOT NULL, `kilograms` FLOAT NOT NULL, `user_profile_id` BIGINT NOT NULL, CONSTRAINT `food_offerPK` PRIMARY KEY (`id`)) ENGINE=InnoDB;
CREATE TABLE `food_portion_trade` (`id` BIGINT AUTO_INCREMENT NOT NULL, `version` BIGINT NOT NULL, `food_id` BIGINT NOT NULL, `from_engaging` bit NOT NULL, `kilograms` FLOAT NOT NULL, `trade_id` BIGINT NOT NULL, CONSTRAINT `food_portion_PK` PRIMARY KEY (`id`)) ENGINE=InnoDB;
CREATE TABLE `trade` (`id` BIGINT AUTO_INCREMENT NOT NULL, `version` BIGINT NOT NULL, `date_created` DATETIME NOT NULL, `engaged_agreed` bit NOT NULL, `engaged_positive_review` bit NOT NULL, `engaged_received` bit NOT NULL, `engaged_reviewed` bit NOT NULL, `engaged_sent` bit NOT NULL, `engaged_user_id` BIGINT NOT NULL, `engaging_agreed` bit NOT NULL, `engaging_positive_review` bit NOT NULL, `engaging_received` bit NOT NULL, `engaging_reviewed` bit NOT NULL, `engaging_sent` bit NOT NULL, `engaging_user_id` BIGINT NOT NULL, `last_updated` DATETIME NOT NULL, `status` BIGINT NOT NULL, CONSTRAINT `tradePK` PRIMARY KEY (`id`)) ENGINE=InnoDB;
CREATE TABLE `user_profile` (`id` BIGINT AUTO_INCREMENT NOT NULL, `version` BIGINT NOT NULL, `activation_code` VARCHAR(255) NULL, `active` bit NOT NULL, `address` VARCHAR(500) NOT NULL, `change_password_code` VARCHAR(255) NULL, `email` VARCHAR(250) NOT NULL, `email_confirmed` bit NOT NULL, `has_offers` bit NOT NULL, `latitude` double precision NULL, `longitude` double precision NULL, `name` VARCHAR(250) NOT NULL, `password` VARCHAR(255) NOT NULL, `photo_filename` VARCHAR(255) NULL, `rating` FLOAT NOT NULL, `rating_count` BIGINT NOT NULL, CONSTRAINT `user_profilePK` PRIMARY KEY (`id`)) ENGINE=InnoDB;
CREATE UNIQUE INDEX `login_uniq_1387139031527` ON `admin_user`(`login`);
CREATE UNIQUE INDEX `name_uniq_1387139031538` ON `food`(`name`);
CREATE INDEX `FK6E6FD54C6387C5CE` ON `food_demand`(`user_profile_id`);
CREATE INDEX `FK6E6FD54CFB52814F` ON `food_demand`(`food_id`);
CREATE INDEX `FKFBE94B3B6387C5CE` ON `food_offer`(`user_profile_id`);
CREATE INDEX `FKFBE94B3BFB52814F` ON `food_offer`(`food_id`);
CREATE INDEX `FKF1D810EBA4E5FD85` ON `food_portion_trade`(`trade_id`);
CREATE INDEX `FKF1D810EBFB52814F` ON `food_portion_trade`(`food_id`);
CREATE INDEX `FK697F1646F14505C` ON `trade`(`engaged_user_id`);
CREATE INDEX `FK697F1647D8A7817` ON `trade`(`engaging_user_id`);
CREATE UNIQUE INDEX `email_uniq_1387139031562` ON `user_profile`(`email`);
CREATE UNIQUE INDEX `name_uniq_1387139031564` ON `user_profile`(`name`);
ALTER TABLE `food_demand` ADD CONSTRAINT `FK6E6FD54CFB52814F` FOREIGN KEY (`food_id`) REFERENCES `food` (`id`);
ALTER TABLE `food_demand` ADD CONSTRAINT `FK6E6FD54C6387C5CE` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profile` (`id`);
ALTER TABLE `food_offer` ADD CONSTRAINT `FKFBE94B3BFB52814F` FOREIGN KEY (`food_id`) REFERENCES `food` (`id`);
ALTER TABLE `food_offer` ADD CONSTRAINT `FKFBE94B3B6387C5CE` FOREIGN KEY (`user_profile_id`) REFERENCES `user_profile` (`id`);
ALTER TABLE `food_portion_trade` ADD CONSTRAINT `FKF1D810EBFB52814F` FOREIGN KEY (`food_id`) REFERENCES `food` (`id`);
ALTER TABLE `food_portion_trade` ADD CONSTRAINT `FKF1D810EBA4E5FD85` FOREIGN KEY (`trade_id`) REFERENCES `trade` (`id`);
ALTER TABLE `trade` ADD CONSTRAINT `FK697F1646F14505C` FOREIGN KEY (`engaged_user_id`) REFERENCES `user_profile` (`id`);
ALTER TABLE `trade` ADD CONSTRAINT `FK697F1647D8A7817` FOREIGN KEY (`engaging_user_id`) REFERENCES `user_profile` (`id`);