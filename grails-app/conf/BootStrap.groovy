import urbanfarmtrading.FoodDemand
import grails.util.GrailsUtil
import urbanfarmtrading.AdminUser
import urbanfarmtrading.UserProfile
import urbanfarmtrading.Food
import urbanfarmtrading.util.MyDateUtils
import urbanfarmtrading.FoodOffer
import urbanfarmtrading.FoodType

class BootStrap {

    def init = { servletContext ->
        switch(GrailsUtil.environment){
            case "development":
                initData()
                initDevelopmentData()
                break;
            case "test":
                break
            case "production":
                initData()
                break
        }
    }

    def destroy = {
    }

    private initData() {
        def admin = AdminUser.findByLogin('admin')
        if (!admin) {
            admin = new AdminUser(login: 'admin', name: 'Administrador', password: '123456')
            admin.passwordToHash()
            admin.save(flush: true, failOnError: true)
        }
    }

    private initDevelopmentData() {
        if (UserProfile.count) return

        def cassio = new UserProfile(name: 'Cássio Landim', email: 'x@gmail.com', password: '123456', longitude: -49.2684724, latitude: -16.6865649, hasOffers: true, address: 'Rua L, 68, apto 502')
        cassio.passwordToHash()
        cassio.save(flush: true, failOnError: true)

        def adson = new UserProfile(name: 'Cássio Rocha', email: 'y@gmail.com', password: '123456', longitude: -49.2509114, latitude: -16.6950458, hasOffers: true, address: 'Rua XX, 18, apto 72')
        adson.passwordToHash()
        adson.save(flush: true, failOnError: true)

        def kaique = new UserProfile(name: 'Cássio Kaique', email: 'z@gmail.com', password: '123456', longitude: -49.27, latitude: -16.67, address: 'Rua YY, 876, apto 71')
        kaique.passwordToHash()
        kaique.save(flush: true, failOnError: true)

        def batata = new Food(name: 'Batata', type: FoodType.VEGETABLE).save(flush: true, failOnError: true)
        def alface = new Food(name: 'Alface', type: FoodType.VEGETABLE).save(flush: true, failOnError: true)
        def ervilha = new Food(name: 'Ervilha', type: FoodType.LEGUME).save(flush: true, failOnError: true)
        def tomate = new Food(name: 'Tomate', type: FoodType.FRUIT).save(flush: true, failOnError: true)
        new Food(name: 'Laranja', type: FoodType.FRUIT).save(flush: true, failOnError: true)
        new Food(name: 'Couve', type: FoodType.VEGETABLE).save(flush: true, failOnError: true)
        new Food(name: 'Orégano', type: FoodType.HERB).save(flush: true, failOnError: true)
        new Food(name: 'Morango', type: FoodType.FRUIT).save(flush: true, failOnError: true)
        new Food(name: 'Pepino', type: FoodType.VEGETABLE).save(flush: true, failOnError: true)

        new FoodOffer(userProfile: adson, food: alface, availableUntil: MyDateUtils.date('20/12/2013'), kilograms: 1.5).save(flush: true, failOnError: true)
        new FoodOffer(userProfile: adson, food: batata, availableUntil: MyDateUtils.date('30/12/2013'), kilograms: 3).save(flush: true, failOnError: true)

        new FoodOffer(userProfile: cassio, food: ervilha, availableUntil: MyDateUtils.date('24/12/2013'), kilograms: 1.2).save(flush: true, failOnError: true)

        new FoodDemand(userProfile: kaique, food: ervilha).save(flush: true, failOnError: true)
        new FoodDemand(userProfile: kaique, food: batata).save(flush: true, failOnError: true)
        new FoodDemand(userProfile: kaique, food: tomate).save(flush: true, failOnError: true)

        new FoodDemand(userProfile: cassio, food: alface).save(flush: true, failOnError: true)
        new FoodDemand(userProfile: cassio, food: tomate).save(flush: true, failOnError: true)
    }
}
