class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.${format})?"{
            constraints {
                // apply constraints here
            }
        }

        "/signup"(controller: 'authentication', action: 'create')
        "/info"(controller: 'info', action: 'index')
        "/dashboard/$action?/$id?"(controller: 'userProfileDashboard')
        "/admin/food/$action?/$id?"(controller: 'adminFood')
        "/admin/user/$action?/$id?"(controller: 'adminUser')
        "/admin/dashboard/$action?/$id?"(controller: 'adminDashboard')
        "/"(view:"/authentication/index")
        "500"(view:'/error')
	}
}
