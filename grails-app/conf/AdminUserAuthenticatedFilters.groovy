class AdminUserAuthenticatedFilters {

    def filters = {
        all(controller: 'adminDashboard|adminUser|adminFood', action: '*') {
            before = {
                if (!session.adminUser) {
                    redirect(controller: "adminHome", action: "login")
                    return false
                }
            }
            after = { Map model ->

            }
            afterView = { Exception e ->

            }
        }
    }
}
