class UserProfileAuthenticatedFilters {

    def filters = {
        all(controller: 'userProfileDashboard|foodOffer|foodDemand|userProfile|trade', action: '*') {
            before = {
                if (!session.userProfile) {
                    redirect(controller: 'authentication', action: 'index')
                    return false
                }
            }
            after = { Map model ->

            }
            afterView = { Exception e ->

            }
        }
    }
}
