package urbanfarmtrading

class UserProfile {

    def grailsApplication

    String name
    String email
    String password
    String photoFilename
    String activationCode
    String changePasswordCode
    Boolean active = true
    Boolean hasOffers = false
    Boolean emailConfirmed = false
    Double latitude = 0D
    Double longitude = 0D
    Float rating = 3F
    Long ratingCount = 0L
    String address

    static constraints = {
        name nullable: false, blank: false, unique: true, maxSize: 250
        email blank: false, email: true, unique: true, maxSize: 250
        address maxSize: 500
        password blank: false, password: true
        latitude nullable: true
        longitude nullable: true
        photoFilename nullable: true
        activationCode nullable: true
        changePasswordCode nullable: true
    }

    def beforeInsert() {
        activationCode = UUID.randomUUID().toString()
    }

    public void passwordToHash() {
        password = password.encodeAsSHA256()
    }

    String toString() {
        name
    }

    static mapping = {
        sort "name"
    }

    String getImgSrcBig() {
        if (photoFilename)
            return "http://${grailsApplication.config.aws.bucketName}.${grailsApplication.config.aws.domain}/userprofile/${photoFilename}_600.jpg"
        return null
    }

    String getImgSrcThumbCropped() {
        if (photoFilename)
            return "http://${grailsApplication.config.aws.bucketName}.${grailsApplication.config.aws.domain}/userprofile/${photoFilename}_300.jpg"
        return null
    }

    def updateHasOffers() {
        if (hasOffers) {
            if (!FoodOffer.countByUserProfile(this)) {
                hasOffers = false
                save(flush: true)
            }
        }
    }

    static def searchByEmailLowercase(String emailRandom) {
        if (!emailRandom) return null
        def userProfiles = UserProfile.where {
            lower(email) == emailRandom.toLowerCase()
        }.list()
        return (userProfiles && userProfiles.size() > 0 ? userProfiles.get(0) : null)
    }

    def getGravatarUrl() {
        return "http://www.gravatar.com/avatar/${email.trim().toLowerCase().encodeAsMD5()}"
    }
}
