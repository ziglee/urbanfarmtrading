package urbanfarmtrading

class Trade {

    UserProfile engagingUser
    UserProfile engagedUser
    Boolean engagingAgreed = true
    Boolean engagedAgreed = false
    Boolean engagingSent = false
    Boolean engagedSent = false
    Boolean engagingReceived = false
    Boolean engagedReceived = false
    Boolean engagingReviewed = false
    Boolean engagedReviewed = false
    Boolean engagingPositiveReview = false
    Boolean engagedPositiveReview = false
    TradeStatus status = TradeStatus.WAITING_AGREEMENT
    Date dateCreated
    Date lastUpdated

    String toString() {
        "Engaging: ${engagingUser.name} | Engaged: ${engagedUser.name} | ${status.code}"
    }
}
