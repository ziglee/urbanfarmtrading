package urbanfarmtrading

class Food {

    def grailsApplication

    String name
    String photoFilename
    FoodType type

    static constraints = {
        name nullable: false, blank: false, unique: true, maxSize: 150
        photoFilename nullable: true
    }

    String toString() {
        name
    }

    String getImgSrcBig() {
        if (photoFilename)
            return "http://${grailsApplication.config.aws.bucketName}.${grailsApplication.config.aws.domain}/food/${photoFilename}_600.jpg"
        return null
    }

    String getImgSrcThumbCropped() {
        if (photoFilename)
            return "http://${grailsApplication.config.aws.bucketName}.${grailsApplication.config.aws.domain}/food/${photoFilename}_300.jpg"
        return null
    }
}
