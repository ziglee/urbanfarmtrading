package urbanfarmtrading

class FoodPortionTrade {

    Food food
    Float kilograms
    Boolean fromEngaging = true

    static belongsTo = [trade: Trade]

    static constraints = {
    }

    String toString() {
        "${food.name} ${kilograms}kg"
    }
}
