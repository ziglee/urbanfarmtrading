package urbanfarmtrading

import org.joda.time.DateTime

class FoodOffer {

    Food food
    Float kilograms = 0
    String comment
    Date availableUntil

    static belongsTo = [userProfile: UserProfile]

    static constraints = {
        comment nullable: true, maxSize: 400
        availableUntil min: new DateTime().withTimeAtStartOfDay().toDate()
    }

    String toString() {
        "${userProfile.name} - ${food.name}"
    }
}
