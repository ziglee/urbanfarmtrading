package urbanfarmtrading

class AdminUser {

    String name
    String login
    String password

    static constraints = {
        name blank: false, nullable: false
        login blank: false, nullable: false, unique: true
        password blank: false, password: true
    }

    public void passwordToHash() {
        password = password.encodeAsSHA256()
    }

    String toString() {
        name
    }

    static mapping = {
        sort "name"
    }
}
